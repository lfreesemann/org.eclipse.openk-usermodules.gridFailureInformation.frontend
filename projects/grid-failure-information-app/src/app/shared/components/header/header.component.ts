/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, Output, Input, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { navigateHome } from '@grid-failure-information-app/shared/utility';
import { Globals } from '@grid-failure-information-app/shared/constants/globals';
import { RolesEnum } from '@grid-failure-information-app/shared/constants/enums';
import { GridFailureSandbox } from '@grid-failure-information-app/pages/grid-failure/grid-failure-list/grid-failure.sandbox';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent {
  @Input() userImage: string;
  @Input() userName: string;

  @Output() logout: EventEmitter<any> = new EventEmitter();
  public helpUrl = Globals.HELP_URL;
  public RolesEnum = RolesEnum;

  constructor(public router: Router, public failureSandbox: GridFailureSandbox) {}

  public navigateHome(): void {
    navigateHome(this.router).then(this._winLocReload);
  }

  public navigateToDistributionGroups(): void {
    this.router.navigateByUrl(`/distribution-groups`).then(this._winLocReload);
  }

  public navigateToLogout(): void {
    this.router.navigateByUrl(`/logout`);
  }

  private _winLocReload(): void {
    window.location.reload();
  }
}
