 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
 /**
 * The page model
 *
 * @export
 * @class PageModel
 * @template T
 */
export class PageModel<T> {
  /**
   * Individual content of a page (eg. customers, branches, users...)
   *
   * @type {any[]}
   * @memberof PageModel
   */
  public content: T[] = [];

  /**
   * Informations about the page concerning sorting, pagesize, etc.
   *
   * @type {*}
   * @memberof PageModel
   */
  public pageable?: any = null;

  /**
   * When the content is paginated, how many pages there are in total.
   *
   * @type {number}
   * @memberof PageModel
   */
  public totalPages: number = 0;

  /**
   * How many elements are shown in current page.
   *
   * @type {number}
   * @memberof PageModel
   */
  public numberOfElements: number = 0;

  /**
   * How many elements are there in total
   *
   * @type {number}
   * @memberof PageModel
   */
  public totalElements: number = 0;

  /**
   * How many items are shown per page.
   *
   * @type {number}
   * @memberof PageModel
   */
  public pageSize: number = 0;


  /**
   * Creates an instance of Page.
   *
   * @param {*} [data=null]
   * @memberof PageModel
   */
  public constructor(data: any = null) {
    Object.keys(data || {})
      .filter(property => this.hasOwnProperty(property))
      .forEach(property => (this[property] = data[property]));
  }
}
