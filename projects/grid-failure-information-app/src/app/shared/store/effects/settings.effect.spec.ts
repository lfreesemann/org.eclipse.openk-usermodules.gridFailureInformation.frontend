/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { SettingsEffects } from '@grid-failure-information-app/shared/store/effects/settings.effect';
import { Subject, of, throwError } from 'rxjs';
import { AppConfigApiClient } from '@grid-failure-information-app/app/app-config-api-client';
import { Store } from '@ngrx/store';
import { Settings } from '@grid-failure-information-app/shared/models';
import { take } from 'rxjs/operators';
import * as settingsActions from '@grid-failure-information-app/shared/store/actions/settings.action';
import { InitialEmailContent } from '@grid-failure-information-app/shared/models/settings-initial-email-content.model';

describe('Settings Effects', () => {
  let effects: SettingsEffects;
  let actions$: Subject<any>;
  let apiClient: AppConfigApiClient;
  let store: Store<any>;

  beforeEach(() => {
    apiClient = {
      getPreConfiguration() {},
      getInitialEmailContent() {},
    } as any;
    store = {
      dispatch() {},
    } as any;
    actions$ = new Subject();
    effects = new SettingsEffects(actions$, apiClient, store);
  });

  it('should be truthy', () => {
    expect(effects).toBeTruthy();
  });

  it('should equal loadPreConfigurationSuccess after getPreConfiguration', () => {
    const apiResponse = new Settings({ overviewMapInitialZoom: 6, detailMapInitialZoom: 6, exportChannels: ['test'] });
    spyOn(apiClient, 'getPreConfiguration').and.returnValue(of(apiResponse));
    effects.loadPreConfiguration$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(settingsActions.loadPreConfigurationSuccess({ payload: apiResponse }));
    });
    actions$.next(settingsActions.loadPreConfiguration());
  });

  it('should equal loadPreConfigurationFail in response to getPreConfiguration Error', () => {
    spyOn(apiClient, 'getPreConfiguration').and.returnValue(throwError('x'));
    effects.loadPreConfiguration$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(settingsActions.loadPreConfigurationFail({ payload: 'x' }));
    });
    actions$.next(settingsActions.loadPreConfigurationFail({ payload: '1' }));
  });

  it('should equal loadInitialEmailContentSuccess after getPreConfiguration', () => {
    const apiResponse = new InitialEmailContent({ emailContentCompleteInit: 'test', emailContentPublishInit: 'test', emailContentUpdateInit: 'test' });
    spyOn(apiClient, 'getInitialEmailContent').and.returnValue(of(apiResponse));
    effects.loadInitialEmailContent$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(settingsActions.loadInitialEmailContentSuccess({ payload: apiResponse }));
    });
    actions$.next(settingsActions.loadInitialEmailContent());
  });

  it('should equal loadPreConfigurationFail in response to getPreConfiguration Error', () => {
    spyOn(apiClient, 'getInitialEmailContent').and.returnValue(throwError('x'));
    effects.loadInitialEmailContent$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(settingsActions.loadInitialEmailContentFail({ payload: 'x' }));
    });
    actions$.next(settingsActions.loadInitialEmailContentFail({ payload: '1' }));
  });
});
