/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from '@grid-failure-information-app/shared/components/components.module';
import { ContainersModule } from '@grid-failure-information-app/shared/containers/containers.module';
import { TranslateModule } from '@ngx-translate/core';
import { DirectivesModule } from '@grid-failure-information-app/shared/directives/directives.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { AgGridModule } from 'ag-grid-angular';
import { NgrxFormsModule } from 'ngrx-forms';
import { EffectsModule } from '@ngrx/effects';
import { NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';

import { distributionGroupReducers } from '@grid-failure-information-app/shared/store';
import { DistributionGroupListComponent } from '@grid-failure-information-app/pages/distribution-group/distribution-group-list/distribution-group-list.component';
import { DistributionGroupRoutingModule } from '@grid-failure-information-app/pages/distribution-group/distribution-group-routing.module';
import { DistributionGroupSandbox } from '@grid-failure-information-app/app/pages/distribution-group/distribution-group.sandbox';
import { DistributionGroupsEffects } from '@grid-failure-information-app/shared/store/effects/distribution-groups.effect';
import { DistributionGroupApiClient } from '@grid-failure-information-app/pages/distribution-group/distribution-group-api-client';
import { DistributionGroupService } from '@grid-failure-information-app/pages/distribution-group/distribution-group.service';
import { DistributionGroupResolver } from '@grid-failure-information-app/pages/distribution-group/distribution-group.resolver';
import { DistributionGroupDetailsComponent } from '@grid-failure-information-app/pages/distribution-group/distribution-group-details/distribution-group-details.component';
import { DistributionGroupMembersComponent } from '@grid-failure-information-app/pages/distribution-group/distribution-group-members/distribution-group-members.component';
import { DistributionGroupMemberPostcodesComponent } from '@grid-failure-information-app/app/pages/distribution-group/distribution-group-member-postcodes/distribution-group-member-postcodes.component';
import { DistributionGroupAllowedComponent } from './distribution-group-allowed/distribution-group-allowed.component';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    ContainersModule,
    TranslateModule,
    DirectivesModule,
    ReactiveFormsModule,
    NgrxFormsModule,
    RouterModule,
    FormsModule,
    StoreModule.forFeature('distribution-group', distributionGroupReducers),
    EffectsModule.forFeature([DistributionGroupsEffects]),
    AgGridModule,
    DistributionGroupRoutingModule,
    NgbTypeaheadModule,
  ],
  declarations: [
    DistributionGroupListComponent,
    DistributionGroupDetailsComponent,
    DistributionGroupAllowedComponent,
    DistributionGroupMembersComponent,
    DistributionGroupMemberPostcodesComponent,
  ],
  providers: [DistributionGroupService, DistributionGroupApiClient, DistributionGroupSandbox, DistributionGroupResolver],
})
export class DistributionGroupModule {}
