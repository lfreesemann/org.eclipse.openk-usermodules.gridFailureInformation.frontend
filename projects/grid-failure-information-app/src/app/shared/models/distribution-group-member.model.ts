/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
export class DistributionGroupMember {
  public contactId: string = null;
  public department: string = null;
  public distributionGroup: string = null;
  public distributionGroupUuid: string = null;
  public email: string = null;
  public mainAddress: string = null;
  public id: string = null;
  public name: string = null;
  public mobileNumber: string = null;
  public personType: string = null;
  public salutationType: string = null;
  public postcodeList: string[] = [];

  public constructor(data: any = null) {
    Object.keys(data || {})
      .filter(property => this.hasOwnProperty(property))
      .forEach(property => (this[property] = data[property]));
  }

  //necessary for "Leer" filter to work correctly
  get displayMainAddress(): string {
    return this.mainAddress.trim().length > 0 ? this.mainAddress : null;
  }
}
