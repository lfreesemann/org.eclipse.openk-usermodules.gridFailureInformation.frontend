/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GRID_FAILURE_COLDEF } from '@grid-failure-information-app/pages/grid-failure/grid-failure-list/grid-failure-list-column-definition';
import { GRID_FAILURE_FOR_CONDENSATION_COLDEF } from '@grid-failure-information-app/app/pages/grid-failure/grid-failure-list/grid-failure-list-for-condensation-column-definition';
import { GridFailureSandbox } from '@grid-failure-information-app/pages/grid-failure/grid-failure-list/grid-failure.sandbox';
import { BaseList } from '@grid-failure-information-app/shared/components/base-components/base.list';
import { HeaderCellRendererComponent } from '@grid-failure-information-app/shared/components/cell-renderer/header-cell-renderer/header-cell-renderer.component';
import { ModeEnum, EventTypeEnum, StateEnum, DistributionPublicationStatusEnum } from '@grid-failure-information-app/shared/constants/enums';
import { SetFilterComponent } from '@grid-failure-information-app/shared/filters/ag-grid/set-filter/set-filter.component';
import { UtilService } from '@grid-failure-information-app/shared/utility';
import { ColDef, GridApi, GridOptions, ICellRendererParams } from 'ag-grid-community';
import { from, Subject } from 'rxjs';
import { takeUntil, take, delay } from 'rxjs/operators';
import { Globals } from '@grid-failure-information-app/shared/constants/globals';
import { RolesEnum, VisibilityEnum } from '@grid-failure-information-app/shared/constants/enums';
import { MapOptions } from '@openk-libs/grid-failure-information-map/shared/models/map-options.model';
import * as store from '@grid-failure-information-app/shared/store';
import { Store } from '@ngrx/store';
import { GridFailure } from '@grid-failure-information-app/shared/models';
import { FuzzySetFilterComponent } from '@grid-failure-information-app/shared/filters/ag-grid/fuzzy-set-filter/fuzzy-set-filter.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-grid-failure-list',
  templateUrl: './grid-failure-list.component.html',
  styleUrls: ['./grid-failure-list.component.scss'],
})
export class GridFailureListComponent extends BaseList implements OnInit, OnDestroy {
  public Globals = Globals;
  public RolesEnum = RolesEnum;
  public VisibilityEnum = VisibilityEnum;
  public overviewColumnDefinition: any;
  public condensationColumnDefinition: any;
  public mapOptions: MapOptions = new MapOptions();
  public frameworkComponents: any;
  public view: any = 'list';
  public showCondensationTable: boolean = false;
  public enableSelectionMode: boolean = false;
  public showEditCondensationBtn: boolean = true;
  public condensationEvents$: Subject<any> = new Subject();
  public gridOptionsCondensation: GridOptions = {
    context: {
      eventSubject: this.condensationEvents$,
    },
    suppressLoadingOverlay: true,
    localeText: Globals.LOCALE_TEXT,
  };
  public preConfig$ = this.appState$.select(store.getPreConfiguration);

  private _gridApi: GridApi;
  private _modeEnum = ModeEnum;
  private _eventTypeEnum = EventTypeEnum;

  constructor(public sandbox: GridFailureSandbox, private appState$: Store<store.State>, private _router: Router,
              private _utilService: UtilService, private _translateService: TranslateService) {
    super();
    this.frameworkComponents = {
      setFilterComponent: SetFilterComponent,
      fuzzySetFilterComponent: FuzzySetFilterComponent,
      headerCellRendererComponent: HeaderCellRendererComponent,
    };
  }

  ngOnInit(): void {
    this._setInitialGridOptions();
    this.appState$.select(store.getPreConfiguration).subscribe(mapConfig => {
      this.mapOptions = new MapOptions(mapConfig);
      if (mapConfig && mapConfig.visibilityConfiguration) {
        GRID_FAILURE_COLDEF.forEach((column: ColDef) => {
          column.hide = mapConfig.visibilityConfiguration.fieldVisibility[column['field']] === VisibilityEnum.HIDE;
          column.headerName = this._translateService.instant(column.headerName)
        });
        this.overviewColumnDefinition = GRID_FAILURE_COLDEF;
        GRID_FAILURE_FOR_CONDENSATION_COLDEF.forEach((column: ColDef) => {
          column.hide = mapConfig.visibilityConfiguration.fieldVisibility[column['field']] === VisibilityEnum.HIDE;
          column.headerName = this._translateService.instant(column.headerName)
        });
        this.condensationColumnDefinition = GRID_FAILURE_FOR_CONDENSATION_COLDEF;
      }
    });
  }

  ngOnDestroy(): void {
    this._endSubscriptions$.next(true);
    this.sandbox.endSubscriptions();
    this.clearGridFailureCondensation();
  }

  public initGridFilter(params): void {
    this._gridApi = params.api;
    this.sandbox.gridFailureListSuccess$.pipe(delay(500), takeUntil(this._endSubscriptions$)).subscribe(() => {
      this._gridApi.setFilterModel(this.sandbox.filterOptions.filterModel);
    });
  }

  public doesExternalFilterPass(node): boolean {
    if (this.sandbox.publisherFilterIsActive) {
      return node.data[Globals.STATUS_PUBLICATION_FIELD] === StateEnum.PUBLISHED || node.data[Globals.STATUS_INTERN_FIELD] === StateEnum.QUALIFIED;
    } else if (this.sandbox.qualifierFilterIsActive) {
      return (
        node.data[Globals.STATUS_INTERN_FIELD] === StateEnum.CREATED ||
        node.data[Globals.STATUS_INTERN_FIELD] === StateEnum.PLANNED ||
        node.data[Globals.STATUS_INTERN_FIELD] === StateEnum.UPDATED
      );
    }
  }

  public changeFilter(checked: boolean = true, columnId: string): any {
    this.sandbox.publisherFilterIsActive = checked && columnId === Globals.STATUS_PUBLICATION_FIELD;
    this.sandbox.qualifierFilterIsActive = checked && columnId === Globals.STATUS_INTERN_FIELD;

    this.overviewColumnDefinition = this.overviewColumnDefinition.map(column => {
      const columnId: string = column['field'];
      if (columnId === Globals.STATUS_INTERN_FIELD || columnId === Globals.STATUS_PUBLICATION_FIELD) {
        return { ...column, suppressMenu: checked };
      } else if (columnId === Globals.STATUS_INTERN_FIELD) {
        return { ...column, suppressMenu: checked };
      }

      return column;
    });

    this._gridApi.onFilterChanged();
  }
  public cancelCondesation(): void {
    this.sandbox.cancelCondesation();
  }
  public clearGridFailureCondensation(): void {
    this.sandbox.clearGridFailureCondensation();
    this.showCondensationTable = false;
    this.enableSelectionMode = false;
    this._changeMode();
    if (!this._gridApi.isDestroyed()) {
      this._gridApi.setFilterModel(this.sandbox.filterOptions.filterModel);
    }
  }

  public condenseChoosedGridFailureInformations(): void {
    if (this.sandbox.condensationList.length != 0) {
      this.sandbox.condenseCondensationList();
      this.clearGridFailureCondensation();
    } else {
      this._utilService.displayNotification('EmptyListWarning', 'alert');
    }
  }

  public addCompleteTable(): void {
    let isNotificationAlreadyShown: boolean = false;
    this._gridApi.forEachNodeAfterFilter(node => {
      let publicationStatusIsPublishedOrWithdrawn: boolean =
        node.data.publicationStatus === StateEnum.PUBLISHED || node.data.publicationStatus === StateEnum.WITHDRAWN;
      const emptyArrayExits = !this.sandbox.condensationList || !this.sandbox.condensationList.length;
      if (
        (emptyArrayExits || node.data.branch === this.sandbox.condensationList[0].branch) &&
        !node.data.condensed &&
        !publicationStatusIsPublishedOrWithdrawn
      ) {
        this.sandbox.addItemToCondensationList(node.data);
      } else if (!isNotificationAlreadyShown && !node.data.condensed && !publicationStatusIsPublishedOrWithdrawn) {
        this._utilService.displayNotification('DifferentBranchesWarning', 'alert');
        isNotificationAlreadyShown = true;
      }
    });
  }

  public changeToSelectionMode(): void {
    this.showCondensationTable = true;
    this.enableSelectionMode = true;
    this._changeMode();
  }

  public loadCondensedGridFailures(id: string): void {
    this.sandbox.loadCondensedGridFailures(id);
    this.showCondensationTable = true;
    this.enableSelectionMode = false;
    this._changeMode();
  }

  public navigateToDetails(id: string): void {
    this._router.navigate(['/grid-failures', id]);
  }

  public setMapFilter(): void {
    const filterModel = this._gridApi.getFilterModel();
    const isColumnFilterActive = !!Object.keys(filterModel).length;
    const filteredGridFailureMapList: GridFailure[] = [];
    this._gridApi.forEachNodeAfterFilter(node => {
      filteredGridFailureMapList.push(new GridFailure(node.data));
    });
    this.sandbox.setFilteredGridFailureMapList(filteredGridFailureMapList);
    if (isColumnFilterActive && !!Object.keys(filterModel).length) {
      Object.keys(filterModel).forEach(columnFilter => {
        if (!('filterModel' in this.sandbox.filterOptions)) {
          this.sandbox.filterOptions['filterModel'] = {};
        }
        this.sandbox.filterOptions['filterModel'][columnFilter] = { ...filterModel[columnFilter] };
      });
    } else {
      this.sandbox.filterOptions = {};
    }
  }

  private _setInitialGridOptions(): void {
    this.gridOptions.context = {
      ...this.gridOptions.context,
      icons: { readonly: this.sandbox.permissions.reader },
    };
    this.gridOptions = {
      ...this.gridOptions,
      onFilterChanged: this.setMapFilter.bind(this),
      onGridReady: this.initGridFilter.bind(this),
      doesExternalFilterPass: this.doesExternalFilterPass.bind(this),
      isExternalFilterPresent: () => this.sandbox.publisherFilterIsActive || this.sandbox.qualifierFilterIsActive,
    };
    this.gridOptions.context.eventSubject.pipe(takeUntil(this._endSubscriptions$)).subscribe(event => {
      switch (event.type) {
        case this._eventTypeEnum.Edit:
        case this._eventTypeEnum.Readonly:
          this._router.navigate(['/grid-failures', event.data.id]);
          break;
        case this._eventTypeEnum.Add:
          const newEmptyCondensList: boolean = this.sandbox.condensationList.length === 0 && !this.sandbox.condenseId;
          const newCondensList: boolean = this.sandbox.condensationList.length > 0 && !this.sandbox.condenseId;
          const editEmptyCondensList: boolean = (newEmptyCondensList || this.sandbox.condensationList.length === 0) && !!this.sandbox.condenseId;
          const editCondensList: boolean = !newEmptyCondensList && !editEmptyCondensList && !!this.sandbox.condenseId;
          const gridFailureAlreadyCondensed = event.data.condensed;
          const canAdd: boolean =
            (newEmptyCondensList ||
              (newCondensList && this.sandbox.condensationList[0].branch === event.data.branch) ||
              (editCondensList && this.sandbox.condensationList[0].branch === event.data.branch) ||
              (editEmptyCondensList && this.sandbox.condenseBranch === event.data.branch)) &&
            !gridFailureAlreadyCondensed;
          if (canAdd) {
            this.sandbox.addItemToCondensationList(event.data);
          } else if (gridFailureAlreadyCondensed) {
            break;
          } else {
            this._utilService.displayNotification('DifferentBranchesWarning', 'alert');
          }
          break;
        case this._eventTypeEnum.AddAllItems:
          this.addCompleteTable();
          setTimeout(() => {
            this._gridApi.setFilterModel(this.sandbox.filterOptions.filterModel);
          }, 100);
          break;
        case this._eventTypeEnum.InitialLoad:
          this._changeMode();
          break;
        case this._eventTypeEnum.LoadCondensedItems:
          this.loadCondensedGridFailures(event.data.id);
          break;

        default:
          break;
      }
    });

    this.gridOptionsCondensation.context = { ...this.gridOptionsCondensation.context, icons: { readonly: this.sandbox.permissions.reader } };
    this.gridOptionsCondensation.context.eventSubject.pipe(takeUntil(this._endSubscriptions$)).subscribe(event => {
      switch (event.type) {
        case this._eventTypeEnum.Remove:
          this.sandbox.removeItemFromCondensationList(event.data);
          setTimeout(() => {
            this._gridApi.setFilterModel(this.sandbox.filterOptions.filterModel);
          }, 100);
          break;
        case this._eventTypeEnum.Edit:
        case this._eventTypeEnum.Readonly:
          this._router.navigate(['/grid-failures', event.data.id]);
          break;
        case this._eventTypeEnum.InitialLoad:
          this._changeMode();
          this.showEditCondensationBtn = !(
            !!this.sandbox.condenseStatusIntern &&
            (this.sandbox.condenseStatusIntern === StateEnum.COMPLETED || this.sandbox.condenseStatusIntern === StateEnum.CANCELED)
          );
          break;
        default:
          break;
      }
    });
  }

  private _changeMode(): void {
    this.enableSelectionMode
      ? this.condensationEvents$.next({ eventType: this._modeEnum.CondensationTableSelectionMode })
      : this.condensationEvents$.next({ eventType: this._modeEnum.CondensationTableNoSelectionMode });
    this.enableSelectionMode
      ? this.events$.next({ eventType: this._modeEnum.OverviewTableSelectionMode })
      : this.events$.next({ eventType: this._modeEnum.InitialMode });
  }
}
