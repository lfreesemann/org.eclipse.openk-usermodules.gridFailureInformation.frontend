/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { GridFailureSandbox } from '@grid-failure-information-map-app/app/grid-failure/grid-failure.sandbox';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class MapComponent implements OnInit, OnDestroy {
  title = 'grid-failure-information-map-app';

  @Input('filter-postcode') isFilterPostcode: boolean;

  @Input() set postcode(value: string) {
    this.sandbox.filterGridFailureMapList(value);
  }

  constructor(public sandbox: GridFailureSandbox) {}

  ngOnDestroy(): void {
    this.sandbox.unsubscribe();
  }

  ngOnInit(): void {
    this.isFilterPostcode = this.isFilterPostcode !== undefined;
    this.sandbox.initSandbox(this.isFilterPostcode);
  }
}
