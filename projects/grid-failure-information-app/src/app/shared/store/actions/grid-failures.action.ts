/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { createAction, props } from '@ngrx/store';
import {
  GridFailure,
  FailureBranch,
  FailureClassification,
  FailureState,
  FailureRadius,
  FailureExpectedReason,
  FailureStation,
  FailureHousenumber,
  FailureAddress,
  DistributionGroup,
  PublicationChannel,
} from '@grid-failure-information-app/shared/models';

export interface ILoadGridFailuresSuccess {
  payload: Array<GridFailure>;
}

export interface ILoadGridFailureSuccess {
  payload: GridFailure;
}

export interface ILoadGridFailuresFail {
  payload: string;
}
export const loadGridFailures = createAction('[GridFailures] Load');
export const loadGridFailuresSuccess = createAction('[GridFailures] Load Success', props<ILoadGridFailuresSuccess>());
export const loadGridFailuresFail = createAction('[GridFailures] Load Fail', props<ILoadGridFailuresFail>());

export const loadGridFailureDetail = createAction('[GridFailure Details] Load', props<{ payload: string }>());
export const loadGridFailureDetailSuccess = createAction('[GridFailure Details] Load Success', props<ILoadGridFailureSuccess>());
export const loadGridFailureDetailFail = createAction('[GridFailure Details] Load Fail', props<{ payload: string }>());

export const saveGridFailure = createAction('[GridFailure Details] Save', props<{ payload: { gridFailure: GridFailure; saveForPublish: boolean } }>());
export const saveGridFailureSuccess = createAction('[GridFailure Details] Save Success', props<{ payload: GridFailure }>());
export const saveGridFailureFail = createAction('[GridFailure Details] Save Fail', props<{ payload: string }>());

export const deleteGridFailure = createAction('[GridFailure Details] Delete', props<{ gridFailureId: string }>());
export const deleteGridFailureSuccess = createAction('[GridFailure Details] Delete Success');
export const deleteGridFailureFail = createAction('[GridFailure Details] Delete Fail', props<{ error: string }>());

export const loadGridFailureVersions = createAction('[GridFailureVersions] Load', props<{ payload: string }>());
export const loadGridFailureVersionsSuccess = createAction('[GridFailureVersions] Load Success', props<{ payload: Array<GridFailure> }>());
export const loadGridFailureVersionsFail = createAction('[GridFailureVersions] Load Fail', props<{ payload: string }>());

export const loadGridFailureVersion = createAction('[GridFailureVersion] Load', props<{ gridFailureId: string; versionNumber: number }>());
export const loadGridFailureVersionSuccess = createAction('[GridFailureVersion] Load Success', props<{ payload: GridFailure }>());
export const loadGridFailureVersionFail = createAction('[GridFailureVersion] Load Fail', props<{ payload: string }>());

export const loadGridFailureBranches = createAction('[GridFailureBranches] Load');
export const loadGridFailureBranchesSuccess = createAction('[GridFailureBranches] Load Success', props<{ payload: Array<FailureBranch> }>());
export const loadGridFailureBranchesFail = createAction('[GridFailureBranches] Load Fail', props<{ payload: string }>());

export const loadGridFailureClassifications = createAction('[GridFailureClassifications] Load');
export const loadGridFailureClassificationsSuccess = createAction(
  '[GridFailureClassifications] Load Success',
  props<{ payload: Array<FailureClassification> }>()
);
export const loadGridFailureClassificationsFail = createAction('[GridFailureClassifications] Load Fail', props<{ payload: string }>());

export const loadGridFailureStates = createAction('[GridFailureStates] Load');
export const loadGridFailureStatesSuccess = createAction('[GridFailureStates] Load Success', props<{ payload: Array<FailureState> }>());
export const loadGridFailureStatesFail = createAction('[GridFailureStates] Load Fail', props<{ payload: string }>());

export const loadGridFailureRadii = createAction('[GridFailureRadii] Load');
export const loadGridFailureRadiiSuccess = createAction('[GridFailureRadii] Load Success', props<{ payload: Array<FailureRadius> }>());
export const loadGridFailureRadiiFail = createAction('[GridFailureRadii] Load Fail', props<{ payload: string }>());

export const loadGridFailureExpectedReasons = createAction('[GridFailureFailureExpectedReasons] Load',
  props<{ payload: string }>());
//  (item = { payload: null }) => {
//  return item;
//});
export const loadGridFailureExpectedReasonsSuccess = createAction(
  '[GridFailureFailureExpectedReasons] Load Success',
  props<{ payload: Array<FailureExpectedReason> }>()
);
export const loadGridFailureExpectedReasonsFail = createAction('[GridFailureFailureExpectedReasons] Load Fail', props<{ payload: string }>());

export const postGridFailuresCondensation = createAction('[CondensedGridFailures] Post', props<{ payload: string[] }>());
export const postGridFailuresCondensationSuccess = createAction('[CondensedGridFailures] Post Success');
export const postGridFailuresCondensationFail = createAction('[CondensedGridFailures] Post Fail', props<{ payload: string }>());

export const loadCondensedGridFailures = createAction('[CondensedGridFailures] Load', props<{ payload: string }>());
export const loadCondensedGridFailuresSuccess = createAction('[CondensedGridFailures] Load Success', props<{ payload: Array<GridFailure> }>());
export const loadCondensedGridFailuresFail = createAction('[CondensedGridFailures] Load Fail', props<{ payload: string }>());

export const putGridFailuresCondensation = createAction('[CondensedGridFailures] Put', props<{ gridFailureId: string; payload: string[] }>());
export const putGridFailuresCondensationSuccess = createAction('[CondensedGridFailures] Put Success', props<{ payload: GridFailure[] }>());
export const putGridFailuresCondensationFail = createAction('[CondensedGridFailures] Put Fail', props<{ payload: string }>());

export const loadStations = createAction('[Stations] Load');
export const loadStationsSuccess = createAction('[Stations] Load Success', props<{ payload: Array<FailureStation> }>());
export const loadStationsFail = createAction('[Stations] Load Fail', props<{ payload: string }>());

export const loadGridFailureStations = createAction('[GridFailureStations] Load', props<{ payload: string }>());
export const loadGridFailureStationsSuccess = createAction('[GridFailureStations] Load Success', props<{ payload: Array<FailureStation> }>());
export const loadGridFailureStationsFail = createAction('[GridFailureStations] Load Fail', props<{ payload: string }>());

export const loadHistGridFailureStations = createAction('[HistGridFailureStations] Load', props<{ failureId: string; versionNumber: string }>());
export const loadHistGridFailureStationsSuccess = createAction('[HistGridFailureStations] Load Success', props<{ payload: Array<FailureStation> }>());
export const loadHistGridFailureStationsFail = createAction('[HistGridFailureStations] Load Fail', props<{ payload: string }>());

export const postGridFailureStation = createAction('[GridFailureStation] Post', props<{ gridFailureDetailId: string; station: FailureStation }>());
export const postGridFailureStationSuccess = createAction('[GridFailureStation] Post Success');
export const postGridFailureStationFail = createAction('[GridFailureStation] Post Fail', props<{ payload: string }>());

export const loadGridFailurePolygon = createAction('[loadGridFailurePolygon] Load', props<{ payload: string[] }>());
export const loadGridFailurePolygonSuccess = createAction(
  '[loadGridFailurePolygon] Load Success',
  props<{ payload: Array<{ stationId: string; polygonCoordinatesList: Array<[Number, Number]> }> }>()
);
export const loadGridFailurePolygonFail = createAction('[loadGridFailurePolygon] Load Fail', props<{ payload: string }>());

export const deleteGridFailureStation = createAction('[GridFailureStation] Delete', props<{ gridFailureDetailId: string; stationId: string }>());
export const deleteGridFailureStationSuccess = createAction('[GridFailureStation] Delete Success');
export const deleteGridFailureStationFail = createAction('[GridFailureStation] Delete Fail', props<{ payload: string }>());

export const loadAddressPostalcodes = createAction('[AddressPostalcodes] Load', props<{ branch: string; community: string; district: string }>());
export const loadAddressPostalcodesSuccess = createAction('[AddressPostalcodes] Load Success', props<{ payload: Array<string> }>());
export const loadAddressPostalcodesFail = createAction('[AddressPostalcodes] Load Fail', props<{ payload: string }>());

export const loadAllAddressCommunities = createAction('[AllAddressCommunities] Load', props<{ branch: string }>());
export const loadAllAddressCommunitiesSuccess = createAction('[AllAddressCommunities] Load Success', props<{ payload: Array<string> }>());
export const loadAllAddressCommunitiesFail = createAction('[AllAddressCommunities] Load Fail', props<{ payload: string }>());

export const loadAddressDistrictsOfCommunity = createAction('[AddressDistrictsOfCommunity] Load', props<{ branch: string; community: string }>());
export const loadAddressDistrictsOfCommunitySuccess = createAction('[AddressDistrictsOfCommunity] Load Success', props<{ payload: Array<string> }>());
export const loadAddressDistrictsOfCommunityFail = createAction('[AddressDistrictsOfCommunity] Load Fail', props<{ payload: string }>());

export const loadAddressStreets = createAction('[AddressStreets] Load', props<{ branch: string; postcode: string; community: string; district?: string }>());
export const loadAddressStreetsSuccess = createAction('[AddressStreets] Load Success', props<{ payload: Array<string> }>());
export const loadAddressStreetsFail = createAction('[AddressStreets] Load Fail', props<{ payload: string }>());

export const loadAddressHouseNumbers = createAction(
  '[AddressHouseNumbers] Load',
  props<{ branch: string; postcode: string; community: string; street: string }>()
);
export const loadAddressHouseNumbersSuccess = createAction('[AddressHouseNumbers] Load Success', props<{ payload: Array<FailureHousenumber> }>());
export const loadAddressHouseNumbersFail = createAction('[AddressHouseNumbers] Load Fail', props<{ payload: string }>());

export const loadGridFailureAddress = createAction('[GridFailureAddress] Load', props<{ payload: string }>());
export const loadGridFailureAddressSuccess = createAction('[GridFailureAddress] Load Success', props<{ payload: FailureAddress }>());
export const loadGridFailureAddressFail = createAction('[GridFailureAddress] Load Fail', props<{ payload: string }>());

export const loadGridFailureDistributionGroups = createAction('[GridFailureDistributionGroups] Load', props<{ payload: string }>());
export const loadGridFailureDistributionGroupsSuccess = createAction(
  '[GridFailureDistributionGroups] Load Success',
  props<{ payload: Array<DistributionGroup> }>()
);
export const loadGridFailureDistributionGroupsFail = createAction('[GridFailureDistributionGroups] Load Fail', props<{ payload: string }>());

export const createDistributionGroupAssignment = createAction(
  '[DistributionGroupAssignment] Create',
  props<{ gridFailureId: string; newGroup: DistributionGroup }>()
);
export const createDistributionGroupAssignmentSuccess = createAction('[DistributionGroupAssignment] Create Success', props<{ payload: DistributionGroup }>());
export const createDistributionGroupAssignmentFail = createAction('[DistributionGroupAssignment] Create Fail', props<{ payload: string }>());

export const deleteDistributionGroupAssignment = createAction('[DistributionGroupAssignment] Delete', props<{ gridFailureId: string; groupId: string }>());
export const deleteDistributionGroupAssignmentSuccess = createAction('[DistributionGroupAssignment] Delete Success');
export const deleteDistributionGroupAssignmentFail = createAction('[DistributionGroupAssignment] Delete Fail', props<{ payload: string }>());

export const loadGridFailurePublicationChannels = createAction('[PublicationChannels] Load', props<{ payload: string }>());
export const loadGridFailurePublicationChannelsSuccess = createAction('[PublicationChannels] Load Success', props<{ payload: Array<PublicationChannel> }>());
export const loadGridFailurePublicationChannelsFail = createAction('[PublicationChannels] Load Fail', props<{ payload: string }>());

export const createPublicationChannelAssignment = createAction('[PublicationChannelAssignment] Create', props<{ gridFailureId: string; channel: string }>());
export const createPublicationChannelAssignmentSuccess = createAction(
  '[PublicationChannelAssignment] Create Success',
  props<{ payload: PublicationChannel }>()
);
export const createPublicationChannelAssignmentFail = createAction('[PublicationChannelAssignment] Create Fail', props<{ payload: string }>());

export const deletePublicationChannelAssignment = createAction('[PublicationChannelAssignment] Delete', props<{ gridFailureId: string; channel: string }>());
export const deletePublicationChannelAssignmentSuccess = createAction('[PublicationChannelAssignment] Delete Success');
export const deletePublicationChannelAssignmentFail = createAction('[PublicationChannelAssignment] Delete Fail', props<{ payload: string }>());

export const loadFailureReminder = createAction('[GridFailureReminder] Load');
export const loadFailureReminderSuccess = createAction('[GridFailureReminder] Load Success', props<{ payload: boolean }>());
export const loadFailureReminderFail = createAction('[GridFailureReminder] Load Fail', props<{ payload: string }>());

export const loadStationCount = createAction('[StationCount] Load', props<{ stationIds: string[] }>());
export const loadStationCountSuccess = createAction('[StationCount] Load Success', props<{ payload: number }>());
export const loadStationCountFail = createAction('[StationCount] Load Fail', props<{ payload: string }>());
