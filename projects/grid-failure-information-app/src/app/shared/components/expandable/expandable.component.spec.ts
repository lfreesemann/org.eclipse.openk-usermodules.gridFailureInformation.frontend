/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { ExpandableComponent } from '@grid-failure-information-app/shared/components/expandable/expandable.component';

describe('ExpandableComponent', () => {
  let component: ExpandableComponent;

  beforeEach(() => {
    component = new ExpandableComponent();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should do nothing if call ngOnInit and showBodyInitially is false', () => {
    component.showBodyInitially = false;
    component.ngOnInit();
    expect(component.showBody).toBeNull();
    expect(component.isExpandedStatus).toBeFalsy();
  });

  it('should show body if call ngOnInit and showBodyInitially is true', () => {
    component.showBodyInitially = true;
    component.ngOnInit();
    expect(component.showBody).toBe('show');
    expect(component.isExpandedStatus).toBeTruthy();
  });
});
