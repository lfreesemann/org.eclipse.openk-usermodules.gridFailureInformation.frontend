# Installed Packages except angular base packages

## @auth0/angular-jwt
- JSON Web Token helper library for Angular

## @ng-bootstrap/ng-bootstrap
- Angular powered Bootstrap

## @ngrx/effects
- Side effect model for @ngrx/store

## @ngrx/store
- RxJS powered Redux for Angular apps

## @ngrx/store-devtools
- Developer tools for @ngrx/store

## @ngx-translate/core
- The internationalization (i18n) library for Angular.

## @ngx-translate/http-loader
- A loader for ngx-translate that loads translations using http.

## @popperjs/core
- Tooltip and Popover Positioning Engine

## ag-grid-angular
- AG Grid is a fully-featured and highly customizable JavaScript data grid.

## ag-grid-community
- AG Grid is a fully-featured and highly customizable JavaScript data grid.

## angular2-notifications
- A light and easy to use notifications library for Angular 2

## bootstrap
- The most popular front-end framework for developing responsive, mobile first projects on the web.

## core-js
- Standard library

## file-saver
- An HTML5 saveAs() FileSaver implementation

## font-awesome
- The iconic font and CSS framework

## leaflet
- JavaScript library for mobile-friendly interactive maps

## ngrx-forms
- Proper integration of forms in Angular 4 applications using ngrx

## npm
- A package manager for JavaScript

## npm-install-peers
- CLI command to install npm peerDependencies

## reselect
- Selectors for Redux.

## rxjs
- Reactive Extensions for modern JavaScript


