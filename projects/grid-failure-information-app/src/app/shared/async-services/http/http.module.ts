/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { HeaderInterceptor } from './auth.interceptor';
import { HttpService } from './http.service';
import { HttpResponseHandler } from './httpResponseHandler.service';

@NgModule({
  imports: [CommonModule],
})
export class HttpServiceModule {
  static forRoot(): ModuleWithProviders<HttpServiceModule> {
    return {
      ngModule: HttpServiceModule,

      providers: [HttpService, HttpResponseHandler, { provide: HTTP_INTERCEPTORS, useClass: HeaderInterceptor, multi: true }],
    };
  }
}
