/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { VersionInfoSandbox } from '@grid-failure-information-app/shared/components/version-info/version-info.sandbox';

describe('VersionInfoSandbox', () => {
  let component: VersionInfoSandbox;

  beforeEach(async(() => {}));

  beforeEach(() => {
    component = new VersionInfoSandbox();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
