variables:
  FF_USE_FASTZIP: "true" # enable fastzip - a faster zip implementation that also supports level configuration.
  ARTIFACT_COMPRESSION_LEVEL: fastest
  CACHE_COMPRESSION_LEVEL: fastest
  TRANSFER_METER_FREQUENCY: 5s # will display transfer progress every 5 seconds for artifacts and remote caches.
  SONAR_USER_HOME: ${CI_PROJECT_DIR}/.sonar  # Defines the location of the analysis task cache
  GIT_DEPTH: 0  # Tells git to fetch all the branches of the project, required by the analysis task
  CLI_VERSION: 17.1.2
  DOCKER_BUILDKIT: 1 # With BuildKit, you don't need to pull the remote images before building since it caches each build layer in your image registry. Then, when you build the image, each layer is downloaded as needed during the build.
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/"

image: node:20.10.0

#-----------------------------------------------------------------------------------------------------------------------
stages: # map-library (Build, Test, Docker) als auch create-map-elements werden aktuell nicht berücksichtigt!
  - build                 # job: build-main
  - build-sub             # jobs: build-map-app / build-table-app / build-web-comp / build-web-cache
  - test                  # jobs: test-main-app / test-map-app / test-table-app / test-web-cache
  - sonarqube             # noch nicht fertig getestet jobs: sonarqube-main / sonarqube-web-cache
  - dockerimage           # jobs: docker-build-main / docker-build-map-app / docker-build-table-app / docker-build-web-comp / docker-build-web-cache

  ########################################################################################################################
  # Non-Parallel Deploy-Stages
  ########################################################################################################################
  - upload                # only tags

  - Deploy-Main
  - Deploy-Map-App
  - Deploy-Table-App
  - Deploy-Web-Comp
  - Deploy-Web-Cache

  - release               # only tags

  ########################################################################################################################
  # Hidden necessary Stages!
  ########################################################################################################################
  # cache (two jobs)
  # install_dependencies (two jobs)


#-----------------------------------------------------------------------------------------------------------------------
.cache_main:
#-----------------------------------------------------------------------------------------------------------------------
  stage: cache
  cache:
    key:
      files:
        - package-lock.json
      prefix: $CI_COMMIT_REF_SLUG-$CI_PROJECT_DIR
    paths:
      - node_modules/


#-----------------------------------------------------------------------------------------------------------------------
.cache_web_cache:
#-----------------------------------------------------------------------------------------------------------------------
  stage: cache
  cache:
    key:
      files:
        - ./projects/grid-failure-information-web-cache/package-lock.json
      prefix: $CI_COMMIT_REF_SLUG-$CI_PROJECT_DIR-web-cache
    paths:
      - ./projects/grid-failure-information-web-cache/node_modules/


#-----------------------------------------------------------------------------------------------------------------------
.install_dependencies_main:
  #-----------------------------------------------------------------------------------------------------------------------
  stage: install_dependencies
  extends: .cache_main
  before_script:
    - test -d "node_modules" && echo "Found/Exists" || echo "Does not exist"
    - |
      if [[ ! -d node_modules ]]; then
        npm i --cache .npm --prefer-offline
      fi


#-----------------------------------------------------------------------------------------------------------------------
.install_dependencies_web_cache:
  #-----------------------------------------------------------------------------------------------------------------------
  stage: install_dependencies
  extends: .cache_web_cache
  before_script:
    - cd ./projects/grid-failure-information-web-cache
    - test -d "node_modules" && echo "Found/Exists" || echo "Does not exist"
    - |
      if [[ ! -d node_modules ]]; then
        npm i --cache .npm --prefer-offline
      fi


#-----------------------------------------------------------------------------------------------------------------------
build-main:
#-----------------------------------------------------------------------------------------------------------------------
  stage: build
  extends: .install_dependencies_main
  script:
    - npm run sy-pre-start
    - npm run build-main-app
    - npm run sy-post-build
    - find -maxdepth 2 -ls
  artifacts:
    paths:
      - dist/
  rules:
    - changes:
        - projects/grid-failure-information-app/**/*
    - if: $CI_COMMIT_TAG

#-----------------------------------------------------------------------------------------------------------------------
build-map-app:
#-----------------------------------------------------------------------------------------------------------------------
  stage: build-sub
  extends: .install_dependencies_main
  script:
    - npm run build-map-app
    - find -maxdepth 2 -ls
  artifacts:
    paths:
      - dist/
  rules:
    - changes:
        - projects/grid-failure-information-map-app/**/*
    - if: $CI_COMMIT_TAG

#-----------------------------------------------------------------------------------------------------------------------
build-table-app:
#-----------------------------------------------------------------------------------------------------------------------
  stage: build-sub
  extends: .install_dependencies_main
  script:
    - npm run build-table-app
    - find -maxdepth 2 -ls
  artifacts:
    paths:
      - dist/
  rules:
    - changes:
        - projects/grid-failure-information-table-app/**/*
    - if: $CI_COMMIT_TAG

#-----------------------------------------------------------------------------------------------------------------------
build-web-comp:
#-----------------------------------------------------------------------------------------------------------------------
  stage: build-sub
  extends: .install_dependencies_main
  script:
    - npm run build-comp
  artifacts:
    paths:
      - dist/
  rules:
    - changes:
        - projects/grid-failure-information-web-comp/**/*
    - if: $CI_COMMIT_TAG

#-----------------------------------------------------------------------------------------------------------------------
build-web-cache:
#-----------------------------------------------------------------------------------------------------------------------
  stage: build
  extends: .install_dependencies_web_cache
  script:
    - npm run build
    - find -maxdepth 2 -ls
  artifacts:
    paths:
      - ./projects/grid-failure-information-web-cache/dist/
  rules:
    - changes:
        - projects/grid-failure-information-web-cache/**/*
    - if: $CI_COMMIT_TAG



#-----------------------------------------------------------------------------------------------------------------------
test-main-app:
#-----------------------------------------------------------------------------------------------------------------------
  stage: test
  extends: .cache_main
  image: trion/ng-cli-karma:${CLI_VERSION}
  allow_failure: false
  variables:
    DISABLE_PUPPETEER: "true"
  script:
      - npm run test:ci
      #- npm run test-single-run --code-coverage --progress false --watch false
  coverage: '/Statements\s*:\s*([\d\.]+)/'
  artifacts:
    paths:
      - coverage/
    reports:
      junit:
        - unittest/grid-failure-information-app/junit.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage/grid-failure-information-app/cobertura-coverage.xml
  rules:
    - changes:
        - projects/grid-failure-information-app/**/*
    - if: $CI_COMMIT_TAG

#-----------------------------------------------------------------------------------------------------------------------
test-map-app:
#-----------------------------------------------------------------------------------------------------------------------
  stage: test
  extends: .cache_main
  image: trion/ng-cli-karma:${CLI_VERSION}
  allow_failure: false
  variables:
    DISABLE_PUPPETEER: "true"
  script:
    - npm run test-map-app-single-run --code-coverage --progress false --watch false
  artifacts:
    paths:
      - coverage/
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage/grid-failure-information-map-app/cobertura-coverage.xml
  rules:
    - changes:
        - projects/grid-failure-information-map-app/**/*
    - if: $CI_COMMIT_TAG

#-----------------------------------------------------------------------------------------------------------------------
test-table-app:
#-----------------------------------------------------------------------------------------------------------------------
  stage: test
  extends: .cache_main
  image: trion/ng-cli-karma:${CLI_VERSION}
  allow_failure: false
  variables:
    DISABLE_PUPPETEER: "true"
  script:
    - npm run test-table-app-single-run --code-coverage --progress false --watch false
  artifacts:
    paths:
      - coverage/
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage/grid-failure-information-table-app/cobertura-coverage.xml
  rules:
    - changes:
        - projects/grid-failure-information-table-app/**/*
    - if: $CI_COMMIT_TAG

#-----------------------------------------------------------------------------------------------------------------------
test-web-cache:
#-----------------------------------------------------------------------------------------------------------------------
  stage: test
  extends: .cache_web_cache
  image: trion/ng-cli-karma:${CLI_VERSION}
  allow_failure: false
  script:
    - cd ./projects/grid-failure-information-web-cache
    - npm run test:cov --code-coverage --progress false --watch false
  artifacts:
    paths:
      - ./projects/grid-failure-information-web-cache/coverage/
  rules:
    - changes:
        - projects/grid-failure-information-web-cache/**/*
      #if: $CI_PROJECT_NAME == "gridfailureinformation.frontend.web-cache"
    - if: $CI_COMMIT_TAG


#-----------------------------------------------------------------------------------------------------------------------
sonarqube-main:
#-----------------------------------------------------------------------------------------------------------------------
  stage: sonarqube
  image:
    name: sonarsource/sonar-scanner-cli:5.0.1
    entrypoint: [""]
  cache:
    key: ${CI_JOB_NAME}
    paths:
      - .sonar/cache
  script:
    - echo ${CI_PROJECT_DIR}
    - sonar-scanner -Dsonar.qualitygate.wait=true -Dsonar.branch.name="${CI_COMMIT_REF_NAME}"
  allow_failure: true
  dependencies:
    - test-main-app
  rules:
    - changes:
        - projects/grid-failure-information-app/**/*
    - changes:
        - projects/grid-failure-information-map-app/**/*
    - changes:
        - projects/grid-failure-information-table-app/**/*
    - changes:
        - projects/grid-failure-information-web-comp/**/*
    - if: $CI_COMMIT_TAG

#-----------------------------------------------------------------------------------------------------------------------
.sonarqube-web-cache:
#-----------------------------------------------------------------------------------------------------------------------
  stage: sonarqube
  image:
    name: sonarsource/sonar-scanner-cli:5.0.1
    entrypoint: [""]
  cache:
    key: ${CI_JOB_NAME}
    paths:
      - .sonar/cache
  script:
    - echo ${CI_PROJECT_DIR}
    - cd ./projects/grid-failure-information-web-cache
    - sonar-scanner -Dsonar.qualitygate.wait=true -Dsonar.branch.name="${CI_COMMIT_REF_NAME}"
  allow_failure: true
  dependencies:
    - test-web-cache
  rules:
    - changes:
        - projects/grid-failure-information-web-cache/**/*
      if: $CI_PROJECT_NAME == "gridfailureinformation.frontend.web-cache"
    - if: $CI_COMMIT_TAG


#-----------------------------------------------------------------------------------------------------------------------
# Dockerimage
#-----------------------------------------------------------------------------------------------------------------------
.docker-build-script:
  image: docker:20.10.7-git
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - echo PROJECT_DIR $PROJECT_DIR
    - echo DOCKER_FILE $DOCKER_FILE
    - ls -l
    - |
      if [[ "$CI_COMMIT_TAG" == "" ]]; then
        tag="sha-${CI_COMMIT_SHORT_SHA}"
        CI_SOURCE_BRANCH=$(echo $CI_COMMIT_BRANCH | tr '[:upper:]' '[:lower:]')
      else
        tag="$CI_COMMIT_TAG";
        # Get (source) branch when tagging
        #CI_SOURCE_BRANCH=$(git for-each-ref | grep $CI_COMMIT_SHA | grep origin | sed "s/.*\///" | tr '[:upper:]' '[:lower:]')
        CI_SOURCE_BRANCH="tag"
      fi
    - echo current tag ${tag}
    - echo CI_SOURCE_BRANCH $CI_SOURCE_BRANCH
    - REGISTRY_IMAGE_BASE="$CI_REGISTRY_IMAGE/$CI_SOURCE_BRANCH/$IMG_NAME"
    - FINAL_REGISTRY_IMAGE="$CI_REGISTRY_IMAGE/$CI_SOURCE_BRANCH/$IMG_NAME:${tag}"
    - echo "FINAL_REGISTRY_IMAGE=$FINAL_REGISTRY_IMAGE" >> dockerimage.env
    - echo "REGISTRY_IMAGE_BASE=$REGISTRY_IMAGE_BASE" >> dockerimage.env
    - echo "IMAGE_TAG=$tag" >> dockerimage.env
    - echo current FINAL_REGISTRY_IMAGE ${FINAL_REGISTRY_IMAGE}
    - docker build --pull -f $DOCKER_FILE -t "$FINAL_REGISTRY_IMAGE" -t "$REGISTRY_IMAGE_BASE" --cache-from "$REGISTRY_IMAGE_BASE"  --build-arg BUILDKIT_INLINE_CACHE=1 .
    - docker push "$REGISTRY_IMAGE_BASE" --all-tags
  artifacts:
    reports:
      dotenv: dockerimage.env


#-----------------------------------------------------------------------------------------------------------------------
docker-build-main:
#-----------------------------------------------------------------------------------------------------------------------
  stage: dockerimage
  extends: .docker-build-script
  variables:
    IMG_NAME: "gfi-main"
    DOCKER_FILE: "./projects/grid-failure-information-app/Dockerfile_Kubernetes_Main"
  needs:
    - job: build-main
  rules:
    - changes:
        - projects/grid-failure-information-app/**/*
      exists:
        - projects/grid-failure-information-app/Dockerfile_Kubernetes_Main
    - if: $CI_COMMIT_TAG


#-----------------------------------------------------------------------------------------------------------------------
docker-build-map-app:
#-----------------------------------------------------------------------------------------------------------------------
  stage: dockerimage
  extends: .docker-build-script
  variables:
    IMG_NAME: "gfi-map-app"
    DOCKER_FILE: "./projects/grid-failure-information-map-app/Dockerfile_Kubernetes_MapApp"
    PROJECT_DIR: "projects/grid-failure-information-map-app"
  needs:
    - job: build-map-app
  rules:
    - changes:
        - projects/grid-failure-information-map-app/**/*
      exists:
        - projects/grid-failure-information-map-app/Dockerfile_Kubernetes_MapApp
    - if: $CI_COMMIT_TAG


#-----------------------------------------------------------------------------------------------------------------------
docker-build-table-app:
#-----------------------------------------------------------------------------------------------------------------------
  stage: dockerimage
  extends: .docker-build-script
  variables:
    IMG_NAME: "gfi-table-app"
    DOCKER_FILE: "./projects/grid-failure-information-table-app/Dockerfile_Kubernetes_TableApp"
    PROJECT_DIR: "projects/grid-failure-information-table-app"
  needs:
    - job: build-table-app
  rules:
    - changes:
        - projects/grid-failure-information-table-app/**/*
      exists:
        - projects/grid-failure-information-table-app/Dockerfile_Kubernetes_TableApp
    - if: $CI_COMMIT_TAG


#-----------------------------------------------------------------------------------------------------------------------
docker-build-web-cache:
  #-----------------------------------------------------------------------------------------------------------------------
  stage: dockerimage
  extends: .docker-build-script
  variables:
    IMG_NAME: "gfi-web-cache"
    DOCKER_FILE: "./projects/grid-failure-information-web-cache/Dockerfile_Kubernetes_WebCache"
    PROJECT_DIR: "projects/grid-failure-information-web-cache"
  needs:
    - job: build-web-cache
  rules:
    - changes:
        - projects/grid-failure-information-web-cache/**/*
      #if: $CI_PROJECT_NAME == "gridfailureinformation.frontend.web-cache"
      exists:
        - projects/grid-failure-information-web-cache/Dockerfile_Kubernetes_WebCache
    - if: $CI_COMMIT_TAG


#-----------------------------------------------------------------------------------------------------------------------
docker-build-web-comp:
#-----------------------------------------------------------------------------------------------------------------------
  stage: dockerimage
  extends: .docker-build-script
  variables:
    IMG_NAME: "gfi-web-comp"
    DOCKER_FILE: "./projects/grid-failure-information-web-comp/Dockerfile_Kubernetes_WebComp"
    PROJECT_DIR: "projects/grid-failure-information-web-comp"
  needs:
    - job: build-web-comp
  rules:
    - changes:
        - projects/grid-failure-information-web-comp/**/*
      exists:
        - projects/grid-failure-information-web-comp/Dockerfile_Kubernetes_WebComp
    - if: $CI_COMMIT_TAG


#-----------------------------------------------------------------------------------------------------------------------
# Upload Artefakte FE
#-----------------------------------------------------------------------------------------------------------------------
upload_artefacts:
  stage: upload
  image: alpine:3.14.0
  rules:
    - if: $CI_COMMIT_TAG
  before_script:
    - apk add --no-cache git curl bash coreutils zip
  script:
    - echo PACKAGE_REGISTRY_URL $PACKAGE_REGISTRY_URL
    - echo CI_COMMIT_TAG $CI_COMMIT_TAG
    - |
      if [[ -d dist/grid-failure-information-app ]]; then
        echo "main"
        mv dist/grid-failure-information-app mainFE
        zip -r mainFE.zip mainFE
        curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file ./mainFE.zip "${PACKAGE_REGISTRY_URL}${CI_COMMIT_TAG}/artefakte/mainFE.zip"
      fi
    - |
      if [[ -d dist/grid-failure-information-map-app ]]; then
        echo "mapApp"
        mv dist/grid-failure-information-map-app mapAppFE
        zip -r mapAppFE.zip mapAppFE
        curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file ./mapAppFE.zip "${PACKAGE_REGISTRY_URL}${CI_COMMIT_TAG}/artefakte/mapAppFE.zip"
      fi
    - |
      if [[ -d dist/grid-failure-information-table-app ]]; then
        mv dist/grid-failure-information-table-app tableAppFE
        zip -r tableAppFE.zip tableAppFE
        curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file ./tableAppFE.zip "${PACKAGE_REGISTRY_URL}${CI_COMMIT_TAG}/artefakte/tableAppFE.zip"
      fi
    - |
      if [[ -d dist/grid-failure-information-web-comp ]]; then
        mv dist/grid-failure-information-web-comp webCompFE
        zip -r webCompFE.zip webCompFE
        curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file ./webCompFE.zip "${PACKAGE_REGISTRY_URL}${CI_COMMIT_TAG}/artefakte/webCompFE.zip"
      fi
    - |
      if [[ -d projects/grid-failure-information-web-cache/dist ]]; then
        mv projects/grid-failure-information-web-cache/dist webCacheFE
        zip -r webCacheFE.zip webCacheFE
        curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file ./webCacheFE.zip "${PACKAGE_REGISTRY_URL}${CI_COMMIT_TAG}/artefakte/webCacheFE.zip"
      fi


#-----------------------------------------------------------------------------------------------------------------------
# Deploy
#-----------------------------------------------------------------------------------------------------------------------
.deploy-script:
  image: alpine:3.14.0
  cache: {}
  variables:
    GIT_STRATEGY: none
    DEPLOYMENT_FILE: fileDefaultVarPlaceholder
    YAML_IMAGE_NAME: image
  before_script:
    - apk add --no-cache git curl bash coreutils
    - wget https://github.com/mikefarah/yq/releases/download/v4.17.2/yq_linux_amd64 -O /usr/bin/yq && chmod +x /usr/bin/yq
    - yq -V
    - ls -l
    - echo current GITLAB_DEPLOYMENT_REPO_URL {GITLAB_DEPLOYMENT_REPO_URL}
    - git clone https://${CI_USERNAME}:${CI_PUSH_TOKEN}@gitlab.com/${GITLAB_DEPLOYMENT_REPO_URL}
    - cd *
    - git config --global user.email "gitlab@gitlab.com"
    - git config --global user.name "GitLab CI/CD"
  script:
    - ls -l
    - cat ${DEPLOYMENT_FILE}
    - echo FINAL_REGISTRY_IMAGE ${FINAL_REGISTRY_IMAGE}
    - echo YAML_APP_NAME ${YAML_APP_NAME}
    - echo IMAGE_TAG ${IMAGE_TAG}
    - echo REGISTRY_IMAGE_BASE ${REGISTRY_IMAGE_BASE}
    - yq e -i '.apps.[env(YAML_APP_NAME)][env(YAML_IMAGE_NAME)].tag = env(IMAGE_TAG)' ${DEPLOYMENT_FILE}
    - yq e -i '.apps.[env(YAML_APP_NAME)][env(YAML_IMAGE_NAME)].repository = env(REGISTRY_IMAGE_BASE)' ${DEPLOYMENT_FILE}
    - cat ${DEPLOYMENT_FILE}
    - git diff
    - |
      if ! git diff --quiet; then
        git commit -am '[skip ci] Image update'
        exit_code=1; attempts=0; max_retry=10;
        set +e # Disable exit on error
        until [ $exit_code -eq 0 ]
        do
          let ++attempts;
          [[ attempts -ne 1 ]] && sleep 3
          echo attempt $attempts
          now=$(date) && echo $now
          git pull --rebase;
          git push origin main;
          exit_code=$?;
          [[ attempts -eq $max_retry ]] && echo "Gave up after $attempts attempts" && break
        done;
        set -e
      else
        echo "No changes found."
      fi


#------------------------------
# Deploy - QA-Environment
#------------------------------
deploy-qa-main:
  stage: Deploy-Main
  extends: .deploy-script
  variables:
    YAML_APP_NAME: gfi-fe
    DEPLOYMENT_FILE: deployment/applications/values-gfi-qa.yaml
  dependencies:
    - docker-build-main
  rules:
    - changes:
        - projects/grid-failure-information-app/**/*
      if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_COMMIT_TAG

deploy-qa-map-app:
  stage: Deploy-Map-App
  extends: .deploy-script
  variables:
    YAML_APP_NAME: gfi-map-app
    DEPLOYMENT_FILE: deployment/applications/values-gfi-qa.yaml
  dependencies:
    - docker-build-map-app
  rules:
    - changes:
        - projects/grid-failure-information-map-app/**/*
      if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_COMMIT_TAG

deploy-qa-table-app:
  stage: Deploy-Table-App
  extends: .deploy-script
  variables:
    YAML_APP_NAME: gfi-table-app
    DEPLOYMENT_FILE: deployment/applications/values-gfi-qa.yaml
  dependencies:
    - docker-build-table-app
  rules:
    - changes:
        - projects/grid-failure-information-table-app/**/*
      if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_COMMIT_TAG

deploy-qa-web-comp:
  stage: Deploy-Web-Comp
  extends: .deploy-script
  variables:
    YAML_APP_NAME: gfi-web-comp
    DEPLOYMENT_FILE: deployment/applications/values-gfi-qa.yaml
  dependencies:
    - docker-build-web-comp
  rules:
    - changes:
        - projects/grid-failure-information-web-comp/**/*
      if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_COMMIT_TAG

deploy-qa-web-cache:
  stage: Deploy-Web-Cache
  extends: .deploy-script
  variables:
    YAML_APP_NAME: gfi-web-cache
    DEPLOYMENT_FILE: deployment/applications/values-gfi-qa.yaml
  dependencies:
    - docker-build-web-cache
  rules:
    - changes:
        - projects/grid-failure-information-web-cache/**/*
      #if: $CI_PROJECT_NAME == "gridfailureinformation.frontend.web-cache" && $CI_COMMIT_REF_NAME == "master"
      if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_COMMIT_TAG

#------------------------------
# Deploy - DEV-Environment
#------------------------------
deploy-dev-main:
  stage: Deploy-Main
  extends: .deploy-script
  variables:
    YAML_APP_NAME: gfi-fe
    DEPLOYMENT_FILE: deployment/applications/values-gfi-dev.yaml
  dependencies:
    - docker-build-main
  rules:
    - changes:
        - projects/grid-failure-information-app/**/*
      if: $CI_COMMIT_BRANCH == "DEVELOP"

deploy-dev-map-app:
  stage: Deploy-Map-App
  extends: .deploy-script
  variables:
    YAML_APP_NAME: gfi-map-app
    DEPLOYMENT_FILE: deployment/applications/values-gfi-dev.yaml
  dependencies:
    - docker-build-map-app
  rules:
    - changes:
        - projects/grid-failure-information-map-app/**/*
      if: $CI_COMMIT_BRANCH == "DEVELOP"

deploy-dev-table-app:
  stage: Deploy-Table-App
  extends: .deploy-script
  variables:
    YAML_APP_NAME: gfi-table-app
    DEPLOYMENT_FILE: deployment/applications/values-gfi-dev.yaml
  dependencies:
    - docker-build-table-app
  rules:
    - changes:
        - projects/grid-failure-information-table-app/**/*
      if: $CI_COMMIT_BRANCH == "DEVELOP"

deploy-dev-web-comp:
  stage: Deploy-Web-Comp
  extends: .deploy-script
  variables:
    YAML_APP_NAME: gfi-web-comp
    DEPLOYMENT_FILE: deployment/applications/values-gfi-dev.yaml
  dependencies:
    - docker-build-web-comp
  rules:
    - changes:
        - projects/grid-failure-information-web-comp/**/*
      if: $CI_COMMIT_BRANCH == "DEVELOP"

deploy-dev-web-cache:
  stage: Deploy-Web-Cache
  extends: .deploy-script
  variables:
    YAML_APP_NAME: gfi-web-cache
    DEPLOYMENT_FILE: deployment/applications/values-gfi-dev.yaml
  dependencies:
    - docker-build-web-cache
  rules:
    - changes:
        - projects/grid-failure-information-web-cache/**/*
      if: $CI_COMMIT_BRANCH == "DEVELOP"


#------------------------------
# Release
#------------------------------
release_job:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
    - job: upload_artefacts
      artifacts: false
  only:
    - tags
  script:
    - echo 'running release_job'
  release:
    name: 'Release $CI_COMMIT_TAG'
    description: 'Created using the release-cli'
    tag_name: '$CI_COMMIT_TAG'
    ref: '$CI_COMMIT_SHA'
    assets:
      links:
        - name: 'mainFE.zip (Download)'
          url:   "${PACKAGE_REGISTRY_URL}${CI_COMMIT_TAG}/artefakte/mainFE.zip"
        - name: 'mapAppFE.zip (Download)'
          url:   "${PACKAGE_REGISTRY_URL}${CI_COMMIT_TAG}/artefakte/mapAppFE.zip"
        - name: 'tableAppFE.zip (Download)'
          url:   "${PACKAGE_REGISTRY_URL}${CI_COMMIT_TAG}/artefakte/tableAppFE.zip"
        - name: 'webCompFE.zip (Download)'
          url:   "${PACKAGE_REGISTRY_URL}${CI_COMMIT_TAG}/artefakte/webCompFE.zip"
        - name: 'webCacheFE.zip (Download)'
          url:   "${PACKAGE_REGISTRY_URL}${CI_COMMIT_TAG}/artefakte/webCacheFE.zip"
