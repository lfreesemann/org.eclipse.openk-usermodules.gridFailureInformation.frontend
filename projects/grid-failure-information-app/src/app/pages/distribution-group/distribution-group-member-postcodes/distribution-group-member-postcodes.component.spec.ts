/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { DistributionGroupSandbox } from '@grid-failure-information-app/app/pages/distribution-group/distribution-group.sandbox';
import { DistributionGroupMemberPostcodesComponent } from '@grid-failure-information-app/app/pages/distribution-group/distribution-group-member-postcodes/distribution-group-member-postcodes.component';
import { DistributionGroupMember } from '@grid-failure-information-app/shared/models';
import { Subscription, of, Subject } from 'rxjs';
import { UtilService } from '@grid-failure-information-app/shared/utility/utility.service';

describe('DistributionGroupMemberPostcodesComponent', () => {
  let component: DistributionGroupMemberPostcodesComponent;
  let sandbox: DistributionGroupSandbox;
  let utilService: UtilService;
  let actionsSubject = { pipe: () => of({}) } as any;
  let contactModelChangedSubject = { pipe: () => of({}), next: () => of({}) } as any;

  beforeEach(() => {
    utilService = { displayNotification() {} } as any;
    sandbox = {
      getSelectedDistributionGroupMember() {
        return new DistributionGroupMember();
      },
      displayPostcodeNotNumberNotification() {},
      updateDistributionGroupMember() {},
      deletePostcode() {},
      addPostcode() {},
      actionsSubject: actionsSubject,
      contactModelChangedSubject$: contactModelChangedSubject,
    } as any;
    component = new DistributionGroupMemberPostcodesComponent(sandbox, utilService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call appropriate function for delete event', () => {
    const spy: any = spyOn(sandbox as any, 'deletePostcode');
    component.ngOnInit();
    component.gridOptions.context.eventSubject.next({ type: 'delete', data: new DistributionGroupMember() });
    expect(spy).toHaveBeenCalled();
  });

  it('should not call deletePostcode method for other than delete event', () => {
    const spy: any = spyOn(sandbox as any, 'deletePostcode');
    component.ngOnInit();
    component.gridOptions.context.eventSubject.next({ type: 'test', data: new DistributionGroupMember() });
    expect(spy).not.toHaveBeenCalled();
  });

  it('should call appropriate function for updating meber with new postcode when called with numeric input', () => {
    const spy: any = spyOn(sandbox, 'addPostcode');
    const input = '07744';
    component.assignPostcode(input);
    expect(spy).toHaveBeenCalled();
  });

  it('should call appropriate function for updating meber with new postcode when called with non-numeric input', () => {
    const spy: any = spyOn(sandbox, 'displayPostcodeNotNumberNotification');
    const input = '0774A';
    component.assignPostcode(input);
    expect(spy).toHaveBeenCalled();
  });

  it('should display Notification if no postcode input is provided', () => {
    const spy: any = spyOn(component['_utilService'], 'displayNotification');
    const input = null;
    component.assignPostcode(input);
    expect(spy).toHaveBeenCalled();
  });

  it('should clear postcode input', () => {
    component.postcodeInput = { nativeElement: { value: 'x' } };
    component.clearPostcodeInput();
    expect(component.postcodeInput.nativeElement.value).toBeFalsy();
  });

  it('should unsubscribe OnDestroy', () => {
    const spy: any = spyOn(component['_endSubscriptions$'], 'next' as any);
    component.ngOnDestroy();
    expect(spy).toHaveBeenCalled();
  });
});
