/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/ import { DistributionGroup } from '@grid-failure-information-app/shared/models';
import { of } from 'rxjs';
import { DistributionGroupAllowedComponent } from './distribution-group-allowed.component';

describe('DistributionGroupAllowedComponent', () => {
  let component: DistributionGroupAllowedComponent;
  let distributionGroupSandbox: any;

  beforeEach(() => {
    distributionGroupSandbox = { getGridFailureClassificationsData$: of({}), getGridFailureBranchesData$: of({}) };
    component = new DistributionGroupAllowedComponent(distributionGroupSandbox);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should ngOnInit', () => {
    const spy = spyOn(component, 'setFormState');

    component.ngOnInit();

    expect(spy).toHaveBeenCalled();
  });

  it('should setFormState', () => {
    component.internGroups = [new DistributionGroup()];

    component.setFormState();

    expect(component.form).toBeDefined();
  });

  it('should addRow', () => {
    const spy = spyOn(component, 'setFormState');
    component.internGroups = [new DistributionGroup()];

    component.addRow();

    expect(component.internGroups.length).toBe(2);
    expect(spy).toHaveBeenCalled();
  });

  it('should delete with id', () => {
    const spy = spyOn(component, 'setFormState');
    let group = {};
    group['id'] = 'id_1';
    group['deleted'] = false;

    component.allowedGroups = [group];
    const rowId = 0;
    component.delete(rowId);

    expect(component.allowedGroups.length).toBe(1);
    expect(spy).toHaveBeenCalled();
  });

  it('should delete without id', () => {
    const spy = spyOn(component, 'setFormState');
    let group = {};
    group['deleted'] = false;

    component.allowedGroups = [group];
    const rowId = 0;
    component.delete(rowId);

    expect(component.allowedGroups.length).toBe(0);
    expect(spy).toHaveBeenCalled();
  });

  it('should ngAfterViewChecked', done => {
    const spy = spyOn(component, 'setFormState');
    let group = {};
    group['deleted'] = false;
    component.allowedGroups = [group];
    component.internGroups = [];

    component.ngAfterViewChecked();
    setTimeout(() => {
      expect(component.internGroups).toEqual(component.allowedGroups);
      expect(spy).toHaveBeenCalled();
      done();
    });
  });
});
