// /********************************************************************************
//  * Copyright (c) 2020 Contributors to the Eclipse Foundation
//  *
//  * See the NOTICE file(s) distributed with this work for additional
//  * information regarding copyright ownership.
//  *
//  * This program and the accompanying materials are made available under the
//  * terms of the Eclipse Public License v. 2.0 which is available at
//  * http://www.eclipse.org/legal/epl-2.0.
//  *
//  * SPDX-License-Identifier: EPL-2.0
//  ********************************************************************************/
import { DateTimeCellRendererComponent } from '@grid-failure-information-app/shared/components/cell-renderer/date-time-cell-renderer/date-time-cell-renderer.component';
import { dateTimeComparator, stringInsensitiveComparator } from '@grid-failure-information-table-app/app/utilityHelpers';

export const APP_TABLE_COLDEF = [
  {
    field: 'branchDescription',
    headerName: 'Sparte',
    filter: 'agTextColumnFilter',
  },
  {
    field: 'failureBegin',
    headerName: 'Beginn der Maßnahme',
    filter: 'agDateColumnFilter',
    filterParams: {
      comparator: dateTimeComparator,
      browserDatePicker: true,
      buttons: ['apply', 'reset'],
      closeOnApply: false,
    },
    cellRenderer: DateTimeCellRendererComponent,
  },
  {
    field: 'failureEndPlanned',
    headerName: 'vor. Ende',
    filter: 'agDateColumnFilter',
    filterParams: {
      comparator: dateTimeComparator,
      browserDatePicker: true,
      buttons: ['apply', 'reset'],
      closeOnApply: false,
    },
    cellRenderer: DateTimeCellRendererComponent,
  },
  {
    field: 'expectedReasonText',
    headerName: 'vor. Grund',
    filter: 'agTextColumnFilter',
    comperator: stringInsensitiveComparator,
  },
  {
    field: 'description',
    headerName: 'Beschreibung',
    filter: 'agTextColumnFilter',
    comperator: stringInsensitiveComparator,
  },
  {
    field: 'postcode',
    headerName: 'PLZ',
    filter: 'agTextColumnFilter',
    valueGetter: params => (params.data.postcode ? params.data.postcode : params.data.freetextPostcode),
  },
  {
    field: 'city',
    headerName: 'Ort',
    filter: 'agTextColumnFilter',
    comperator: stringInsensitiveComparator,
    valueGetter: params => (params.data.city ? params.data.city : params.data.freetextCity),
  },
  {
    field: 'district',
    headerName: 'Ortsteil',
    filter: 'agTextColumnFilter',
    comperator: stringInsensitiveComparator,
    valueGetter: params => (params.data.district ? params.data.district : params.data.freetextDistrict),
  },
  {
    field: 'street',
    headerName: 'Straße',
    comperator: stringInsensitiveComparator,
    filter: 'agTextColumnFilter',
  },
  {
    field: 'failureClassification',
    headerName: 'Meldungsart',
    filter: 'agTextColumnFilter',
  },
];
