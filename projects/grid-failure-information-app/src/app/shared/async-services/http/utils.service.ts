/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HttpService } from '@grid-failure-information-app/shared/async-services/http/http.service';

export function methodBuilder(method: string) {
  return function(url: string) {
    return function(target: HttpService, propertyKey: string, descriptor: any) {
      const pPath = target[`${propertyKey}_Path_parameters`],
        pQuery = target[`${propertyKey}_Query_parameters`],
        pBody = target[`${propertyKey}_Body_parameters`],
        pHeader = target[`${propertyKey}_Header_parameters`];

      descriptor.value = function(...args: any[]) {
        const body: string = createBody(pBody, descriptor, args);
        const resUrl: string = createPath(url, pPath, args);
        const search = createQuery(pQuery, args);
        const headers: HttpHeaders = createHeaders(pHeader, descriptor, this.getDefaultHeaders(), args);

        const http: HttpClient = this.http as HttpClient;

        let responseType = 'json' as 'json'
        if (descriptor.headers) {
          for (const value of Object.values(descriptor.headers)) {
            if (value.toString().includes('text')) {
              responseType = 'text' as 'json';
              break;
            }
          }
        }

        // Request options
        const options = {
          params: search,
          headers: headers,
          responseType: responseType,
        };
        // make the request and store the observable for later transformation
        let observable: Observable<Object>;

        switch (method) {
          case "GET":
            observable = http.get(this.getBaseUrl() + resUrl, options);
            break;
          case "POST":
            observable = http.post(this.getBaseUrl() + resUrl, body, options);
            break;
          case "PUT":
            observable = http.put(this.getBaseUrl() + resUrl, body, options);
            break;
          case "DELETE":
            observable = http.delete(this.getBaseUrl() + resUrl, options);
            break;
          case "HEAD":
            observable = http.head(this.getBaseUrl() + resUrl, options);
            break;
        }

        // intercept the response
        observable = this.responseInterceptor(observable, descriptor.adapter);

        return observable;
      };

      return descriptor;
    };
  };
}

export function paramBuilder(paramName: string) {
  return function(key: string) {
    return function(target: HttpService, propertyKey: string | symbol, parameterIndex: number) {
      const metadataKey = `${String(propertyKey)}_${paramName}_parameters`;
      const paramObj: any = {
        key: key,
        parameterIndex: parameterIndex,
      };

      if (Array.isArray(target[metadataKey])) {
        target[metadataKey].push(paramObj);
      } else {
        target[metadataKey] = [paramObj];
      }
    };
  };
}

function createBody(pBody: Array<any>, descriptor: any, args: Array<any>): string {
  if (descriptor.isFormData) {
    return args[0];
  }
  return pBody ? JSON.stringify(args[pBody[0].parameterIndex]) : null;
}

function createPath(url: string, pPath: Array<any>, args: Array<any>): string {
  let resUrl: string = url;

  if (pPath) {
    for (const k in pPath) {
      if (pPath.hasOwnProperty(k)) {
        resUrl = resUrl.replace('{' + pPath[k].key + '}', args[pPath[k].parameterIndex]);
      }
    }
  }

  return resUrl;
}

function createQuery(pQuery: any, args: Array<any>) {
  const search = {}
  if (pQuery) {
    pQuery
      .filter(p => args[p.parameterIndex]) // filter out optional parameters
      .forEach(p => {
        const key = p.key;
        let value = args[p.parameterIndex];
        // if the value is a instance of Object, we stringify it
        if (value instanceof Object) {
          value = JSON.stringify(value);
        }
        search[encodeURIComponent(key)] = encodeURIComponent(value);
      });
  }

  return search;
}

function createHeaders(pHeader: any, descriptor: any, defaultHeaders: any, args: Array<any>): HttpHeaders {
  if (descriptor.headers) {
    defaultHeaders = descriptor.headers;
  }

  const headers = new HttpHeaders(defaultHeaders);
  const authorization = 'Authorization';
  if (headers.has(authorization)) {
    headers.delete(authorization);
  }
  headers.append(authorization, localStorage.getItem('token'));
  // set method specific headers
  for (const k in descriptor.headers) {
    if (descriptor.headers.hasOwnProperty(k)) {
      if (headers.has(k)) {
        headers.delete(k);
      }
      headers.append(k, descriptor.headers[k]);
    }
  }

  // set parameter specific headers
  if (pHeader) {
    for (const k in pHeader) {
      if (pHeader.hasOwnProperty(k)) {
        if (headers.has(k)) {
          headers.delete(k);
        }
        headers.append(pHeader[k].key, args[pHeader[k].parameterIndex]);
      }
    }
  }

  return headers;
}
