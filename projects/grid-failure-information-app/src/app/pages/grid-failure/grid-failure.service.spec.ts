/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import {
  GridFailure,
  FailureBranch,
  FailureClassification,
  FailureState,
  FailureRadius,
  FailureExpectedReason,
} from '@grid-failure-information-app/shared/models';
import { GridFailureService } from '@grid-failure-information-app/pages/grid-failure/grid-failure.service';

describe(' GridFailureService', () => {
  beforeEach(() => {});

  it('should transform list', () => {
    const response = [new GridFailure()];
    response[0].id = 'X';

    expect(GridFailureService.gridAdapter(response)[0].id).toBe('X');
  });

  it('should transform details', () => {
    const response: any = { id: 'X' };
    const item = GridFailureService.itemAdapter(response);

    expect(item.id).toBe(response.id);
  });

  it('should transform branches api response', () => {
    const response = [new FailureBranch()];
    response[0].id = 'X';

    expect(GridFailureService.branchListAdapter(response)[0].id).toBe('X');
  });

  it('should transform classifications api response', () => {
    const response = [new FailureClassification()];
    response[0].id = 'X';

    expect(GridFailureService.classificationListAdapter(response)[0].id).toBe('X');
  });

  it('should transform states api response', () => {
    const response = [new FailureState()];
    response[0].id = 'X';

    expect(GridFailureService.stateListAdapter(response)[0].id).toBe('X');
  });

  it('should transform radii api response', () => {
    const response = [new FailureRadius()];
    response[0].id = 'X';

    expect(GridFailureService.radiusListAdapter(response)[0].id).toBe('X');
  });

  it('should transform expected reason api response', () => {
    const response = [new FailureExpectedReason()];
    response[0].id = 'X';

    expect(GridFailureService.expectedReasonListAdapter(response)[0].id).toBe('X');
  });

  it('should transform address item list response', () => {
    const response = ['X'];
    expect(GridFailureService.addressItemListAdapter(response)[0]).toBe('X');
  });

  it('should transform address item response', () => {
    const response: any = { id: 'X' };
    const item = GridFailureService.addressAdapter(response);

    expect(item.id).toBe(response.id);
  });

  it('should transform housenumber list response', () => {
    const response: any = [{ uuid: 'X' }];
    expect(GridFailureService.housenumberListAdapter(response)[0].uuid).toBe('X');
  });

  it('should transform station list response', () => {
    const response: any = [{ id: 'X' }];
    expect(GridFailureService.stationListAdapter(response)[0].id).toBe('X');
  });

  it('should transform station item response', () => {
    const response: any = { id: 'X' };
    const item = GridFailureService.stationAdapter(response);

    expect(item.id).toBe(response.id);
  });

  it('should transform distribution group list response', () => {
    const response: any = [{ id: 'X' }];
    expect(GridFailureService.distributionGroupListAdapter(response)[0].id).toBe('X');
  });

  it('should transform distribution group response', () => {
    const response: any = { id: 'X' };
    const item = GridFailureService.distributionGroupAdapter(response);
    expect(item.id).toBe(response.id);
  });

  it('should transform  publication channel list response', () => {
    const response: any = [{ id: 'X' }];
    expect(GridFailureService.publicationChannelListAdapter(response)[0].id).toBe('X');
  });

  it('should transform publication channel response', () => {
    const response: any = { id: 'X' };
    const item = GridFailureService.publicationChannelAdapter(response);
    expect(item.id).toBe(response.id);
  });

  it('should not transform reminder response', () => {
    const response: any = true;
    const test = GridFailureService.reminderAdapter(response);
    expect(test).toBe(response);
  });
});
