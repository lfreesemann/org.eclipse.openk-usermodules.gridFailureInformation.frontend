/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { SafetyQueryDialogComponent } from '@grid-failure-information-app/shared/components/dialogs/safety-query-dialog/safety-query-dialog.component';
import { DistributionPublicationStatusEnum, DistributionsGroupTextTypeEnum } from '@grid-failure-information-app/shared/constants/enums';
import {
  Contact,
  DistributionGroup,
  DistributionGroupMember,
  DistributionGroupTextPlaceholder,
  FailureBranch,
  FailureClassification,
} from '@grid-failure-information-app/shared/models';
import { InitialEmailContent } from '@grid-failure-information-app/shared/models/settings-initial-email-content.model';
import { BaseSandbox } from '@grid-failure-information-app/shared/sandbox/base.sandbox';
import * as store from '@grid-failure-information-app/shared/store';
import * as distributionGroupActions from '@grid-failure-information-app/shared/store/actions/distribution-groups.action';
import * as gridFailureActions from '@grid-failure-information-app/shared/store/actions/grid-failures.action';
import * as distributionGroupFormReducer from '@grid-failure-information-app/shared/store/reducers/distribution-groups/distribution-group-details-form.reducer';
import { INITIAL_STATE } from '@grid-failure-information-app/shared/store/reducers/distribution-groups/distribution-group-details-form.reducer';
import { UtilService } from '@grid-failure-information-app/shared/utility/utility.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ofType } from '@ngrx/effects';
import { ActionsSubject, Store } from '@ngrx/store';
import { CellClickedEvent } from 'ag-grid-community';
import { saveAs } from 'file-saver';
import { FormGroupState, MarkAsTouchedAction, ResetAction, SetValueAction } from 'ngrx-forms';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, switchMap, take, takeUntil } from 'rxjs/operators';

export interface PostcodeInterface {
  postcode: string;
}

@Injectable()
export class DistributionGroupSandbox extends BaseSandbox {
  public getGridFailureBranchesData$: Observable<FailureBranch[]> = this.appState$.select(store.getGridFailureBranchesData);
  public getGridFailureClassificationsData$: Observable<FailureClassification[]> = this.appState$.select(store.getGridFailureClassificationsData);
  public distributionGroupList$: Observable<DistributionGroup[]> = this.appState$.select(store.getDistributionGroupsData);
  public distributionGroupLoading$: Observable<boolean> = this.appState$.select(store.getDistributionGroupsLoading);
  public distributionGroupDetailsFormState$: Observable<FormGroupState<DistributionGroup>> = this.appState$.select(store.getDistributionGroupsDetails);
  public currentFormState: FormGroupState<DistributionGroup>;
  public distributionGroupMember$: Observable<DistributionGroupMember[]> = this.appState$.select(store.getDistributionGroupMembersData).pipe();
  public distributionGroupMemberLoading$: Observable<boolean> = this.appState$.select(store.getDistributionGroupMembersLoading);
  public disableMemberButton: boolean = false;
  public distributionGroupTextPlaceholder: DistributionGroupTextPlaceholder;
  public selectedPublicationStatusForTemplate: DistributionPublicationStatusEnum = DistributionPublicationStatusEnum.PUBLISH;
  public oldPublicationStatusForTemplate: DistributionPublicationStatusEnum;
  public selectedTextTypeForTemplate: DistributionsGroupTextTypeEnum = DistributionsGroupTextTypeEnum.Long;
  public oldTextTypeForTemplate: DistributionsGroupTextTypeEnum;
  public selectedMemberRowIndex: number = 0;
  public postcodes: PostcodeInterface[] = [];
  public selectedDistributionGroup: DistributionGroup;
  public contactModelChangedSubject$: Subject<any> = new Subject();
  public currentAllowedGroup = [];

  private _selectedContact: Contact;
  private _distributionGroupMembers: Array<DistributionGroupMember>;
  private _selectFirst: boolean = true;
  private _selectedDistributionGroupMember: DistributionGroupMember;

  constructor(
    protected appState$: Store<store.State>,
    public actionsSubject: ActionsSubject,
    private _utilService: UtilService,
    private _modalService: NgbModal
  ) {
    super(appState$);
    this.registerEvents();
  }

  public createDistributionGroup(): void {
    this.appState$.dispatch(distributionGroupActions.resetDistributionGroupMembers());
    this._clear();
    this.appState$.dispatch(new MarkAsTouchedAction(distributionGroupFormReducer.FORM_ID));
    this.selectedPublicationStatusForTemplate = DistributionPublicationStatusEnum.PUBLISH;
    this.oldPublicationStatusForTemplate = DistributionPublicationStatusEnum.PUBLISH;
    this.oldTextTypeForTemplate = DistributionsGroupTextTypeEnum.Long;
    this.appState$.select(store.getInitialEmailContent).subscribe((initialEmailContent: InitialEmailContent) => {
      this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectView.id, initialEmailContent.emailSubjectPublishInit));
      this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.distributionTextView.id, initialEmailContent.emailContentPublishInit));
      this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectViewShort.id, initialEmailContent.emailSubjectPublishShortInit));
      this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.distributionTextViewShort.id, initialEmailContent.emailContentPublishShortInit));

      // set long text type - E-Mail
      this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectPublish.id, initialEmailContent.emailSubjectPublishInit));
      this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectUpdate.id, initialEmailContent.emailSubjectUpdateInit));
      this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectComplete.id, initialEmailContent.emailSubjectCompleteInit));
      this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.distributionTextPublish.id, initialEmailContent.emailContentPublishInit));
      this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.distributionTextUpdate.id, initialEmailContent.emailContentUpdateInit));
      this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.distributionTextComplete.id, initialEmailContent.emailContentCompleteInit));

      // set short text type - SMS
      this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectPublishShort.id, initialEmailContent.emailSubjectPublishShortInit));
      this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectUpdateShort.id, initialEmailContent.emailSubjectUpdateShortInit));
      this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectCompleteShort.id, initialEmailContent.emailSubjectCompleteShortInit));
      this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.distributionTextPublishShort.id, initialEmailContent.emailContentPublishShortInit));
      this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.distributionTextUpdateShort.id, initialEmailContent.emailContentUpdateShortInit));
      this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.distributionTextCompleteShort.id, initialEmailContent.emailContentCompleteShortInit));
    });
    this.clearAssignmentInput();
    this.disableMemberButton = true;
    this.postcodes = [];
  }

  public loadDistributionGroups(): void {
    this.appState$.dispatch(distributionGroupActions.loadDistributionGroups());
  }

  public loadDistributionGroupDetail(id: string): void {
    this.selectedPublicationStatusForTemplate = DistributionPublicationStatusEnum.PUBLISH;
    this.selectedTextTypeForTemplate = DistributionsGroupTextTypeEnum.Long;
    this.appState$.dispatch(distributionGroupActions.loadDistributionGroupsDetail({ payload: id }));
  }

  public loadDistributionGroupTextPlaceholders(): void {
    this.appState$.dispatch(distributionGroupActions.loadDistributionGroupTextPlaceholders());
  }

  public saveDistributionGroup(): void {
    // Copy user control value to right model property for correct validation
    switch (this.selectedPublicationStatusForTemplate) {
      case DistributionPublicationStatusEnum.PUBLISH:
        // If DistributionsGroupTextTypeEnum is Long then set the template for E-Mail otherwise set it for SMS
        if (this.selectedTextTypeForTemplate === DistributionsGroupTextTypeEnum.Long) {
          this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectPublish.id, this.currentFormState.value.emailSubjectView));
          this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.distributionTextPublish.id, this.currentFormState.value.distributionTextView));
        } else {
          this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectPublishShort.id, this.currentFormState.value.emailSubjectViewShort));
          this.appState$.dispatch(
            new SetValueAction(INITIAL_STATE.controls.distributionTextPublishShort.id, this.currentFormState.value.distributionTextViewShort)
          );
        }
        break;
      case DistributionPublicationStatusEnum.COMPLETE:
        // If DistributionsGroupTextTypeEnum is Long then set the template for E-Mail otherwise set it for SMS
        if (this.selectedTextTypeForTemplate === DistributionsGroupTextTypeEnum.Long) {
          this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectComplete.id, this.currentFormState.value.emailSubjectView));
          this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.distributionTextComplete.id, this.currentFormState.value.distributionTextView));
        } else {
          this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectCompleteShort.id, this.currentFormState.value.emailSubjectViewShort));
          this.appState$.dispatch(
            new SetValueAction(INITIAL_STATE.controls.distributionTextCompleteShort.id, this.currentFormState.value.distributionTextViewShort)
          );
        }
        break;
      case DistributionPublicationStatusEnum.UPDATE:
        // If DistributionsGroupTextTypeEnum is Long then set the template for E-Mail otherwise set it for SMS
        if (this.selectedTextTypeForTemplate === DistributionsGroupTextTypeEnum.Long) {
          this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectUpdate.id, this.currentFormState.value.emailSubjectView));
          this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.distributionTextUpdate.id, this.currentFormState.value.distributionTextView));
        } else {
          this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectUpdateShort.id, this.currentFormState.value.emailSubjectViewShort));
          this.appState$.dispatch(
            new SetValueAction(INITIAL_STATE.controls.distributionTextUpdateShort.id, this.currentFormState.value.distributionTextViewShort)
          );
        }
        break;
    }
    if (this.currentFormState.isValid) {
      this.changeEmailTemplate(this.selectedPublicationStatusForTemplate, this.selectedPublicationStatusForTemplate, this.selectedTextTypeForTemplate);
      let distributionGroup = new DistributionGroup(this.currentFormState.value);

      // update: id exists
      if (distributionGroup && distributionGroup.id) {
        this.selectedDistributionGroup = distributionGroup;
      }
      this.actionsSubject
        .pipe(
          ofType(distributionGroupActions.saveDistributionGroupSuccess),
          take(1),
          map(action => action.payload),
          takeUntil(this._endSubscriptions$)
        )
        .subscribe(item => {
          //post case: new group with  is returned; put case: general response without id is returned
          if (item && item.id) {
            this.selectedDistributionGroup = item;
          }
          this.selectedPublicationStatusForTemplate = DistributionPublicationStatusEnum.PUBLISH;
          this.oldPublicationStatusForTemplate = DistributionPublicationStatusEnum.PUBLISH;
          this.selectedTextTypeForTemplate = DistributionsGroupTextTypeEnum.Long;
          this.oldTextTypeForTemplate = DistributionsGroupTextTypeEnum.Long;
          this._clear();
        });

      this.appState$.dispatch(
        distributionGroupActions.saveDistributionGroup({
          payload: distributionGroup,
        })
      );
    } else {
      this._utilService.displayNotification('MandatoryFieldError', 'alert');
    }
  }

  public deleteDistributionGroup(id: string): void {
    const modalRef = this._modalService.open(SafetyQueryDialogComponent);
    modalRef.componentInstance.title = 'ConfirmDialog.Action.delete';
    modalRef.componentInstance.body = 'ConfirmDialog.Deletion';
    modalRef.result.then(
      () => {
        this.actionsSubject.pipe(ofType(distributionGroupActions.deleteDistributionGroupSuccess), take(1)).subscribe(() => {
          this._selectFirst = true;
        });
        this.appState$.dispatch(distributionGroupActions.deleteDistributionGroup({ payload: id }));
      },
      () => {}
    );
  }

  public deleteDistributionGroupMember(groupId: string, memberId: string): void {
    this.actionsSubject.pipe(ofType(distributionGroupActions.deleteDistributionGroupMemberSuccess), take(1)).subscribe(() => (this.selectedMemberRowIndex = 0));
    this.appState$.dispatch(distributionGroupActions.deleteDistributionGroupMember({ groupId: groupId, memberId: memberId }));
  }

  public updateDistributionGroupMember(postcodeList: string[]): void {
    let displayMainAddress: string = null;
    if (
      this._selectedDistributionGroupMember &&
      this._selectedDistributionGroupMember.mainAddress &&
      this._selectedDistributionGroupMember.mainAddress.trim().length > 0
    ) {
      displayMainAddress = this._selectedDistributionGroupMember.mainAddress;
    }
    this._selectedDistributionGroupMember = {
      ...this._selectedDistributionGroupMember,
      postcodeList: postcodeList,
      displayMainAddress: displayMainAddress,
    };
    this.appState$.dispatch(
      distributionGroupActions.updateDistributionGroupMember({
        groupId: this.selectedDistributionGroup.id,
        memberId: this._selectedDistributionGroupMember.id,
        member: this._selectedDistributionGroupMember,
      })
    );
  }

  public registerEvents(): void {
    this.distributionGroupDetailsFormState$.pipe(takeUntil(this._endSubscriptions$)).subscribe((formState: FormGroupState<DistributionGroup>) => {
      this.currentFormState = formState;
    });
    this.actionsSubject
      .pipe(
        ofType(distributionGroupActions.loadDistributionGroupsSuccess),
        map(action => action.payload),
        takeUntil(this._endSubscriptions$)
      )
      .subscribe((groups: Array<DistributionGroup>) => {
        if (groups && groups.length > 0) {
          if (!!this.selectedDistributionGroup && !this._selectFirst) {
            this.appState$.dispatch(distributionGroupActions.loadDistributionGroupsDetail({ payload: this.selectedDistributionGroup.id }));
            this.appState$.dispatch(distributionGroupActions.loadDistributionGroupMembers({ payload: this.selectedDistributionGroup.id }));
          } else {
            this.selectedDistributionGroup = groups[0];
            this.appState$.dispatch(distributionGroupActions.loadDistributionGroupsDetail({ payload: groups[0].id }));
            this.appState$.dispatch(distributionGroupActions.loadDistributionGroupMembers({ payload: groups[0].id }));
            this._selectFirst = false;
          }
        }
      });
    this.actionsSubject
      .pipe(
        ofType(distributionGroupActions.loadDistributionGroupMembersSuccess),
        map(action => action.payload),
        takeUntil(this._endSubscriptions$)
      )
      .subscribe((members: Array<DistributionGroupMember>) => {
        this._distributionGroupMembers = members;
        if (!this._selectedDistributionGroupMember || this.selectedMemberRowIndex === 0) {
          this._selectedDistributionGroupMember = this._distributionGroupMembers[0];
        }
        this.transformPostcodes();
      });
    this.actionsSubject
      .pipe(
        ofType(distributionGroupActions.loadDistributionGroupTextPlaceholdersSuccess),
        map(action => action.payload),
        takeUntil(this._endSubscriptions$)
      )
      .subscribe((item: DistributionGroupTextPlaceholder) => {
        this.distributionGroupTextPlaceholder = item;
      });
    this.actionsSubject
      .pipe(
        ofType(distributionGroupActions.exportContactsSuccess),
        map(action => action.payload),
        takeUntil(this._endSubscriptions$)
      )
      .subscribe((response: any) => {
        let filename: string;
        let defaultFilename: string = 'export.txt';
        let filenameContentDisposition: string;
        const responseBody = new Blob([response], { type: 'text/csv;charset=utf-8;' });
        if (response.headers) {
          const contentDisposition = response.headers.get('content-disposition');
          filenameContentDisposition = contentDisposition.split(';')[1].trim().split('=')[1].replace(/"/g, '');
        }
        filename = filenameContentDisposition ? filenameContentDisposition : defaultFilename;
        saveAs(responseBody, filename);
      });
  }

  public showMembersToSelectedGroup(selectedGroup: CellClickedEvent): void {
    if (selectedGroup && selectedGroup.data) {
      this.selectedDistributionGroup = selectedGroup.data;
      this.appState$.dispatch(distributionGroupActions.loadDistributionGroupMembers({ payload: selectedGroup.data.id }));
    }
  }

  public loadContacts(term: string): Observable<any> {
    this.appState$.dispatch(distributionGroupActions.loadContacts({ searchText: term }));
    return this.actionsSubject.pipe(
      ofType(distributionGroupActions.loadContactsSuccess),
      map(action => action.payload),
      takeUntil(this._endSubscriptions$)
    );
  }

  public searchForContacts = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(term => (term.length < 2 ? [] : this.loadContacts(term)))
    );

  public formatter = (s: Contact | string) => {
    if (s instanceof Contact) return s.contactSearchString;
    else return s;
  };

  public setSelectedContact(selectedContact: Contact): void {
    this._selectedContact = selectedContact;
  }

  public assignContactToGroup(): void {
    if (!this.selectedDistributionGroup) return;
    if (!this._selectedContact) {
      this._utilService.displayNotification('NoContactSelected');
      return;
    }
    let testMember: DistributionGroupMember = this._distributionGroupMembers.find(member => member.contactId === this._selectedContact.uuid);
    if (testMember) {
      this._utilService.displayNotification('SelectedContactAlreadyAssigned', 'alert');
      this.setSelectedContact(undefined);
    } else {
      let newGroupMemberData = {
        contactId: this._selectedContact.uuid,
        distributionGroupUuid: this.selectedDistributionGroup.id,
      };
      const newGroupMember = new DistributionGroupMember(newGroupMemberData);
      this.actionsSubject.pipe(ofType(distributionGroupActions.createDistributionGroupMemberSuccess), take(1)).subscribe(payload => {
        this.selectedMemberRowIndex = 0;
      });
      this.appState$.dispatch(
        distributionGroupActions.createDistributionGroupMember({
          groupId: this.selectedDistributionGroup.id,
          newMember: newGroupMember,
        })
      );
      this.postcodes = [];
      this._selectedDistributionGroupMember = newGroupMember;
      this.setSelectedContact(undefined);
    }
  }

  public exportContacts(): void {
    if (!this.selectedDistributionGroup) return;
    this.appState$.dispatch(
      distributionGroupActions.exportContacts({
        groupId: this.selectedDistributionGroup.id,
      })
    );
  }

  public displayPostcodeNotNumberNotification() {
    this._utilService.displayNotification('PostcodeNotNumber', 'alert');
  }

  private _clear(): void {
    this.appState$.dispatch(new SetValueAction(distributionGroupFormReducer.FORM_ID, distributionGroupFormReducer.INITIAL_STATE.value));
    this.appState$.dispatch(new ResetAction(distributionGroupFormReducer.FORM_ID));
    this.disableMemberButton = false;
  }

  public changeEmailTemplate(
    selectedPublicationStatusForTemplate: DistributionPublicationStatusEnum,
    oldPublicationStatusForTemplate: DistributionPublicationStatusEnum = DistributionPublicationStatusEnum.PUBLISH,
    selectedTextTypeForTemplate: DistributionsGroupTextTypeEnum,
    oldTextTypeForTemplate: DistributionsGroupTextTypeEnum = DistributionsGroupTextTypeEnum.Long
  ) {
    let emailsubject: string;
    let distributionText: string;
    let emailsubjectShort: string;
    let distributionTextShort: string;
    let emailsubjectOld: string = this.currentFormState.value.emailSubjectView;
    let distributionTextOld: string = this.currentFormState.value.distributionTextView;
    let emailsubjectShortOld: string = this.currentFormState.value.emailSubjectViewShort;
    let distributionTextShortOld: string = this.currentFormState.value.distributionTextViewShort;

    switch (oldPublicationStatusForTemplate) {
      case DistributionPublicationStatusEnum.COMPLETE:
        // If DistributionsGroupTextTypeEnum is Long then set the template for E-Mail otherwise set it for SMS
        if (oldTextTypeForTemplate === DistributionsGroupTextTypeEnum.Long) {
          this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectComplete.id, emailsubjectOld));
          this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.distributionTextComplete.id, distributionTextOld));
        } else {
          this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectCompleteShort.id, emailsubjectShortOld));
          this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.distributionTextCompleteShort.id, distributionTextShortOld));
        }
        break;
      case DistributionPublicationStatusEnum.PUBLISH:
        // If DistributionsGroupTextTypeEnum is Long then set the template for E-Mail otherwise set it for SMS
        if (oldTextTypeForTemplate === DistributionsGroupTextTypeEnum.Long) {
          this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectPublish.id, emailsubjectOld));
          this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.distributionTextPublish.id, distributionTextOld));
        } else {
          this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectPublishShort.id, emailsubjectShortOld));
          this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.distributionTextPublishShort.id, distributionTextShortOld));
        }
        break;
      case DistributionPublicationStatusEnum.UPDATE:
        // If DistributionsGroupTextTypeEnum is Long then set the template for E-Mail otherwise set it for SMS
        if (oldTextTypeForTemplate === DistributionsGroupTextTypeEnum.Long) {
          this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectUpdate.id, emailsubjectOld));
          this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.distributionTextUpdate.id, distributionTextOld));
        } else {
          this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectUpdateShort.id, emailsubjectShortOld));
          this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.distributionTextUpdateShort.id, distributionTextShortOld));
        }
        break;
      default:
        break;
    }
    switch (selectedPublicationStatusForTemplate) {
      case DistributionPublicationStatusEnum.COMPLETE:
        emailsubject = this.currentFormState.value.emailSubjectComplete;
        distributionText = this.currentFormState.value.distributionTextComplete;
        emailsubjectShort = this.currentFormState.value.emailSubjectCompleteShort;
        distributionTextShort = this.currentFormState.value.distributionTextCompleteShort;
        break;
      case DistributionPublicationStatusEnum.PUBLISH:
        emailsubject = this.currentFormState.value.emailSubjectPublish;
        distributionText = this.currentFormState.value.distributionTextPublish;
        emailsubjectShort = this.currentFormState.value.emailSubjectPublishShort;
        distributionTextShort = this.currentFormState.value.distributionTextPublishShort;
        break;
      case DistributionPublicationStatusEnum.UPDATE:
        emailsubject = this.currentFormState.value.emailSubjectUpdate;
        distributionText = this.currentFormState.value.distributionTextUpdate;
        emailsubjectShort = this.currentFormState.value.emailSubjectUpdateShort;
        distributionTextShort = this.currentFormState.value.distributionTextUpdateShort;
        break;
      default:
        break;
    }
    this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectView.id, emailsubject));
    this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.distributionTextView.id, distributionText));
    this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectViewShort.id, emailsubjectShort));
    this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.distributionTextViewShort.id, distributionTextShort));
  }

  public transformPostcodes() {
    let postcodeStringArray: string[] = [];
    if (this._selectedDistributionGroupMember || (this._distributionGroupMembers && this._distributionGroupMembers.length > 0)) {
      postcodeStringArray = (this._selectedDistributionGroupMember ? this._selectedDistributionGroupMember : this._distributionGroupMembers[0]).postcodeList;
    }
    if (postcodeStringArray.length > 0) {
      let postcodes: PostcodeInterface[] = [];
      for (var index in postcodeStringArray) {
        postcodes.push({ postcode: postcodeStringArray[index] });
      }
      this.postcodes = postcodes;
    } else {
      this.postcodes = [];
    }
  }

  public setSelectedDistributionGroupMember(member: DistributionGroupMember): void {
    this._selectedDistributionGroupMember = member;
  }

  public deletePostcode(postcodeString: string): void {
    this.postcodes = this.postcodes.filter(item => item.postcode != postcodeString);
    this._updateMember(this.postcodes);
  }

  public addPostcode(input: PostcodeInterface) {
    let postcodeAlreadyAssigned = false;
    if (this.postcodes.length > 0) {
      for (var index in this.postcodes) {
        if (this.postcodes[index].postcode === input.postcode) {
          postcodeAlreadyAssigned = true;
        }
      }
    }
    if (postcodeAlreadyAssigned) {
      this._utilService.displayNotification('PostcodeAlreadyAssigned', 'alert');
    } else {
      this.postcodes = [...this.postcodes, input];
      this._updateMember(this.postcodes);
    }
  }

  public getDistributionGroupMemebers(): Array<DistributionGroupMember> {
    return this._distributionGroupMembers ? this._distributionGroupMembers : [];
  }

  public clearAssignmentInput(): void {
    this.appState$.dispatch(distributionGroupActions.clearAssignmentInput());
  }

  public loadGridFailureBranches(): void {
    this.appState$.dispatch(gridFailureActions.loadGridFailureBranches());
  }

  public loadGridFailureClassifications(): void {
    this.appState$.dispatch(gridFailureActions.loadGridFailureClassifications());
  }

  private _updateMember(postcodes: PostcodeInterface[]) {
    let postcodeStringArray: string[] = [];
    if (postcodes.length > 0) {
      for (var index in postcodes) {
        postcodeStringArray.push(postcodes[index].postcode);
      }
    }
    this.updateDistributionGroupMember(postcodeStringArray);
  }
}
