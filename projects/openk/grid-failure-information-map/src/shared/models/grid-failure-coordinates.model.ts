/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { unbox } from 'ngrx-forms';
import { Globals } from '@openk-libs/grid-failure-information-map/constants/globals';

export class GridFailureMapInformation {
  public id: string = null;
  public longitude: number = null;
  public latitude: number = null;
  public radius: number = null;
  public addressPolygonPoints: Array<{ stationId: string; polygonCoordinatesList: Array<[Number, Number]> }> = null;
  public failureBegin: string = null;
  public failureEndPlanned: string = null;
  public expectedReasonText: string = null;
  public branchDescription: string = null;
  public postcode: string = null;
  public city: string = null;
  public district: string = null;
  public freetextPostcode: string = null;
  public freetextCity: string = null;
  public freetextDistrict: string = null;

  public constructor(data: any = null) {
    Object.keys(data || {})
      .filter(property => this.hasOwnProperty(property))
      .forEach(property => {
        if (Globals.PROPERTIES_TO_BOX.includes(property)) {
          this[property] = unbox(data[property]);
        } else {
          this[property] = data[property];
        }
      });
  }
}
