/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { GridFailureSandbox } from '@grid-failure-information-map-app/app/grid-failure/grid-failure.sandbox';
import { GridFailureService } from '@grid-failure-information-map-app/app/grid-failure/grid-failure.service';
import { AppConfigService } from '@grid-failure-information-map-app/app/app-config.service';
import { of, Subject } from 'rxjs';
import { GridFailure, Settings } from '@grid-failure-information-app/shared/models';
import { VisibilityEnum } from '@grid-failure-information-app/shared/constants/enums';

describe('GridFailureSandbox', () => {
  let sandbox: GridFailureSandbox;
  let gridFailureService: GridFailureService;
  let configService: AppConfigService;
  let gridFailureMapListAll: GridFailure[] = [];
  let gridFailureMapList: Subject<GridFailure[]> = new Subject<GridFailure[]>();

  beforeEach(() => {
    let config = new Settings();
    config.dataExternInitialVisibility = VisibilityEnum.SHOW;
    configService = {
      getConfig: () => of(config),
    } as any;
    gridFailureService = {
      getGridFailureData: () => of([new GridFailure()]),
    } as any;
    gridFailureMapListAll = [new GridFailure({ postcode: '007' }), new GridFailure({ postcode: '4711' }), new GridFailure({ postcode: '0815' })];
    gridFailureMapList.next(gridFailureMapListAll);
    sandbox = new GridFailureSandbox(gridFailureService, configService);
    (sandbox as any)._gridFailureMapListAll = gridFailureMapListAll;
    sandbox.gridFailureMapList$ = gridFailureMapList;
  });

  it('should create an instance', () => {
    expect(sandbox).toBeTruthy();
  });

  it('should unsubscribe subscriptions', () => {
    const spy: any = spyOn(sandbox['_subscription'], 'unsubscribe');
    sandbox.unsubscribe();
    expect(spy).toHaveBeenCalled();
  });

  it('filterGridFailureMapList() should assign all gridfailure to map property', () => {
    sandbox.initSandbox(false);
    let count = 0;
    sandbox.gridFailureMapList$.subscribe(gridFailures => {
      count = gridFailures.length;
    });
    sandbox.filterGridFailureMapList();
    expect(count).toEqual(0);
  });

  it('filterGridFailureMapList("") should assign all gridfailure to map property', () => {
    sandbox.initSandbox(false);
    let count = 0;
    sandbox.gridFailureMapList$.subscribe(gridFailures => {
      count = gridFailures.length;
    });

    sandbox.filterGridFailureMapList('');
    expect(count).toEqual(0);
  });

  it('filterGridFailureMapList("007") should assign only one gridfailure to map property', () => {
    let count = 0;
    sandbox.gridFailureMapList$.subscribe(gridFailures => {
      count = gridFailures.length;
    });
    sandbox.filterGridFailureMapList('007');
    expect(count).toEqual(0);
  });
});
