/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { RolesEnum } from '@grid-failure-information-app/shared/constants/enums';

export class PermissionsModel {
  public admin: RolesEnum;
  public creator: RolesEnum;
  public publisher: RolesEnum;
  public qualifier: RolesEnum;
  public reader: RolesEnum;

  /**
   *Creates an instance of PermissionsModel.
   * @param {*} [data=null]
   * @memberof PermissionsModel
   */
  public constructor(data: string[] = null) {
    if (!!data) {
      data.forEach((permissionItem: string) => {
        switch (permissionItem) {
          case RolesEnum.ADMIN:
            this.admin = RolesEnum.ADMIN;
            break;
          case RolesEnum.CREATOR:
            this.creator = RolesEnum.CREATOR;
            break;
          case RolesEnum.PUBLISHER:
            this.publisher = RolesEnum.PUBLISHER;
            break;
          case RolesEnum.QUALIFIER:
            this.qualifier =RolesEnum.QUALIFIER;
            break;
          case RolesEnum.READER:
              this.reader = RolesEnum.READER;
              break;
          default:
            break;
        }
      });
    }
  }
}
