/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import * as utilityHelpers from '@grid-failure-information-app/shared/utility/utilityHelpers';
import { FailureHousenumber } from '@grid-failure-information-app/shared/models';
import { VisibilityConfigurationInterface } from '@grid-failure-information-app/shared/interfaces/visibility-configuration.interface';

describe('utilityHelpers', () => {
  it('should navigate to overview after call navigateHome', () => {
    const router = { navigateByUrl() {} } as any;
    const spy = spyOn(router, 'navigateByUrl');
    utilityHelpers.navigateHome(router);
    expect(spy).toHaveBeenCalledWith('/grid-failures');
  });

  it('should parse string to Integer via toInteger()', () => {
    let testValue = utilityHelpers.toInteger('1');
    expect(testValue).toBe(parseInt(`${1}`, 10));
  });

  it('should return true if a value is a number', () => {
    let testValue = utilityHelpers.isNumber('1');
    expect(testValue).toBe(true);
  });

  it('should return false if a value is not a number', () => {
    let testValue = utilityHelpers.isNumber('a');
    expect(testValue).toBe(false);
  });

  it('should pad the number if a value is a number', () => {
    let testValue = utilityHelpers.padNumber(1);
    expect(testValue).toBe('01');
  });

  it('should return empty string if a value is not a number', () => {
    let testValue = utilityHelpers.padNumber(null);
    expect(testValue).toBe('');
  });

  it('should convert a string date to a german date string', () => {
    const dateString: string = '2020-12-01';
    let testValue = utilityHelpers.localeDateString(dateString);
    expect(testValue).toBe('1.12.2020');
  });

  it('should compare two dates and return 0 if equal', () => {
    const cellValue: string = '2020-04-20';
    const filterLocalDateAtMidnight: Date = new Date(2020, 3, 20);
    let testValue = utilityHelpers.dateTimeComparator(filterLocalDateAtMidnight, cellValue);
    expect(testValue).toBe(0);
  });

  it('should compare two dates and return -1 if cellDate less than filterLocalDateAtMidnight', () => {
    const cellValue: string = '2020-04-20T00:00:00.000Z';
    const filterLocalDateAtMidnightString: string = '2021-04-20T00:00:00.000Z';
    const filterLocalDateAtMidnight: Date = new Date(filterLocalDateAtMidnightString);
    let testValue = utilityHelpers.dateTimeComparator(filterLocalDateAtMidnight, cellValue);
    expect(testValue).toBe(-1);
  });

  it('should compare two dates and return 1 if cellDate greater than filterLocalDateAtMidnight', () => {
    const cellValue: string = '2021-04-20T00:00:00.000Z';
    const filterLocalDateAtMidnightString: string = '2020-04-20T00:00:00.000Z';
    const filterLocalDateAtMidnight: Date = new Date(filterLocalDateAtMidnightString);
    let testValue = utilityHelpers.dateTimeComparator(filterLocalDateAtMidnight, cellValue);
    expect(testValue).toBe(1);
  });

  it('should return the dependency property from the controlid string', () => {
    const controlId: string = 'controlId.dependencyProperty';
    let testValue = utilityHelpers.getDependencyPropertyFromControlId(controlId);
    expect(testValue).toBe('dependencyProperty');
  });

  it('should convert empty string to NULL in convertEmptyValueToNull(..)', () => {
    let testValue = utilityHelpers.convertEmptyValueToNull('  ');
    expect(testValue).toBe(null);
  });

  it('should return null when insert null in convertEmptyValueToNull(..)', () => {
    let testValue = utilityHelpers.convertEmptyValueToNull(null);
    expect(testValue).toBe(null);
  });

  it('should return string when insert the same string in convertEmptyValueToNull(..)', () => {
    const inputText = 'test';
    let testValue = utilityHelpers.convertEmptyValueToNull(inputText);
    expect(testValue).toBe(inputText);
  });

  it('should sort two strings', () => {
    const inputText = '1b';
    const inputText1 = '1';
    let testValue = utilityHelpers.sortAlphaNum(inputText, inputText1);
    expect(testValue).toBe(1);

    const inputText2 = '123';
    testValue = utilityHelpers.sortAlphaNum(inputText1, inputText2);
    expect(testValue).toBe(-1);

    const inputFailureHousenumber = new FailureHousenumber({ housenumber: '123' });
    const inputFailureHousenumber2 = new FailureHousenumber({ housenumber: '123' });
    testValue = utilityHelpers.sortAlphaNum(inputFailureHousenumber, inputFailureHousenumber2);
    expect(testValue).toBe(0);
  });

  it('should return true for detailFieldVisibility in case no config is provided', () => {
    let config: VisibilityConfigurationInterface = null;
    let testValue = utilityHelpers.determineDetailFieldVisibility(config, 'fieldVisibility', 'description');
    expect(testValue).toBeTruthy();
  });

  it('should determine addressRelevan Branch', () => {
    let testValue = utilityHelpers.getAddressRelevantBranch('S');
    expect(testValue).toBe('S');
  });
});
