/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { AppTableService } from '@grid-failure-information-table-app/app/app-table.service';

describe('AppTableService', () => {
  let service: AppTableService;
  let mockHttpClient;

  beforeEach(() => {
    mockHttpClient = { get: () => {} };
    service = new AppTableService(mockHttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get data via loadGridFailureData()', () => {
    const spy: any = spyOn(service['http'], 'get');
    service.loadGridFailureData();
    expect(spy).toHaveBeenCalled();
  });
});
