/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GridFailure } from '@grid-failure-information-app/shared/models';

@Injectable({
  providedIn: 'root',
})
export class AppTableService {
  constructor(private http: HttpClient) {}
  loadGridFailureData(): Observable<GridFailure[]> {
    return this.http.get<GridFailure[]>('public-sit');
  }
}
