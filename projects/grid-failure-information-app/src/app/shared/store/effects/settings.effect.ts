/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import * as settingsActions from '@grid-failure-information-app/shared/store/actions/settings.action';
import { catchError, map, switchMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppConfigApiClient } from '@grid-failure-information-app/app/app-config-api-client';

@Injectable()
export class SettingsEffects {

  loadPreConfiguration$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(settingsActions.loadPreConfiguration),
      map(action => action['payload']),
      switchMap(() => {
        return this._apiClient.getPreConfiguration().pipe(
          map(item => settingsActions.loadPreConfigurationSuccess({ payload: item })),
          catchError(error => of(settingsActions.loadPreConfigurationFail({ payload: error })))
        );
      })
    )
  );

  loadInitialEmailContent$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(settingsActions.loadInitialEmailContent),
      map(action => action['payload']),
      switchMap(() => {
        return this._apiClient.getInitialEmailContent().pipe(
          map(item => settingsActions.loadInitialEmailContentSuccess({ payload: item })),
          catchError(error => of(settingsActions.loadInitialEmailContentFail({ payload: error })))
        );
      })
    )
  );

  constructor(private _actions$: Actions, private _apiClient: AppConfigApiClient, private _store: Store<any>) {}
}
